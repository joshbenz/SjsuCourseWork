#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RequestVoteRequest {
    #[prost(uint64, tag = "1")]
    pub term: u64,
    #[prost(uint32, tag = "2")]
    pub candidate_id: u32,
    #[prost(uint64, tag = "3")]
    pub last_log_index: u64,
    #[prost(uint64, tag = "4")]
    pub last_log_term: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RequestVoteResponse {
    #[prost(uint64, tag = "1")]
    pub term: u64,
    #[prost(bool, tag = "2")]
    pub vote_granted: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AppendEntriesRequest {
    #[prost(uint64, tag = "1")]
    pub term: u64,
    #[prost(uint32, tag = "2")]
    pub leader_id: u32,
    #[prost(uint64, tag = "3")]
    pub prev_log_index: u64,
    #[prost(uint64, tag = "4")]
    pub prev_log_term: u64,
    #[prost(message, optional, tag = "5")]
    pub entry: ::std::option::Option<Entry>,
    #[prost(uint64, tag = "6")]
    pub leader_commit: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AppendEntriesResponse {
    #[prost(uint64, tag = "1")]
    pub term: u64,
    #[prost(bool, tag = "2")]
    pub success: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Entry {
    #[prost(uint64, tag = "1")]
    pub term: u64,
    #[prost(uint64, tag = "2")]
    pub index: u64,
    #[prost(string, tag = "3")]
    pub decree: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ClientAppendRequest {
    #[prost(string, tag = "1")]
    pub decree: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ClientAppendResponse {
    /// 0 means success, 1 means not the leader
    #[prost(uint32, tag = "1")]
    pub rc: u32,
    #[prost(uint64, tag = "2")]
    pub leader: u64,
    #[prost(uint64, tag = "3")]
    pub index: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ClientRequestIndexRequest {
    #[prost(uint64, tag = "1")]
    pub index: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ClientRequestIndexResponse {
    /// 0 means success, 1 means not the leader
    #[prost(uint32, tag = "1")]
    pub rc: u32,
    #[prost(uint64, tag = "2")]
    pub leader: u64,
    /// this will be the requested index or the last committed
    /// index if the requested index is not found
    #[prost(uint64, tag = "3")]
    pub index: u64,
    #[prost(string, tag = "4")]
    pub decree: std::string::String,
}
#[doc = r" Generated client implementations."]
pub mod client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct RaftServerClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl RaftServerClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> RaftServerClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
        <T::ResponseBody as HttpBody>::Data: Into<bytes::Bytes> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        #[doc = r" Check if the service is ready."]
        pub async fn ready(&mut self) -> Result<(), tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })
        }
        pub async fn request_vote(
            &mut self,
            request: impl tonic::IntoRequest<super::RequestVoteRequest>,
        ) -> Result<tonic::Response<super::RequestVoteResponse>, tonic::Status> {
            self.ready().await?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/raft.RaftServer/RequestVote");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn append_entries(
            &mut self,
            request: impl tonic::IntoRequest<super::AppendEntriesRequest>,
        ) -> Result<tonic::Response<super::AppendEntriesResponse>, tonic::Status> {
            self.ready().await?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/raft.RaftServer/AppendEntries");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " these next two RPCs are sent from client to the leader"]
        pub async fn client_append(
            &mut self,
            request: impl tonic::IntoRequest<super::ClientAppendRequest>,
        ) -> Result<tonic::Response<super::ClientAppendResponse>, tonic::Status> {
            self.ready().await?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/raft.RaftServer/ClientAppend");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn client_request_index(
            &mut self,
            request: impl tonic::IntoRequest<super::ClientRequestIndexRequest>,
        ) -> Result<tonic::Response<super::ClientRequestIndexResponse>, tonic::Status> {
            self.ready().await?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/raft.RaftServer/ClientRequestIndex");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for RaftServerClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with RaftServerServer."]
    #[async_trait]
    pub trait RaftServer: Send + Sync + 'static {
        async fn request_vote(
            &self,
            request: tonic::Request<super::RequestVoteRequest>,
        ) -> Result<tonic::Response<super::RequestVoteResponse>, tonic::Status> {
            Err(tonic::Status::unimplemented("Not yet implemented"))
        }
        async fn append_entries(
            &self,
            request: tonic::Request<super::AppendEntriesRequest>,
        ) -> Result<tonic::Response<super::AppendEntriesResponse>, tonic::Status> {
            Err(tonic::Status::unimplemented("Not yet implemented"))
        }
        #[doc = " these next two RPCs are sent from client to the leader"]
        async fn client_append(
            &self,
            request: tonic::Request<super::ClientAppendRequest>,
        ) -> Result<tonic::Response<super::ClientAppendResponse>, tonic::Status> {
            Err(tonic::Status::unimplemented("Not yet implemented"))
        }
        async fn client_request_index(
            &self,
            request: tonic::Request<super::ClientRequestIndexRequest>,
        ) -> Result<tonic::Response<super::ClientRequestIndexResponse>, tonic::Status> {
            Err(tonic::Status::unimplemented("Not yet implemented"))
        }
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct RaftServerServer<T: RaftServer> {
        inner: Arc<T>,
    }
    impl<T: RaftServer> RaftServerServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            Self { inner }
        }
    }
    impl<T: RaftServer> Service<http::Request<HyperBody>> for RaftServerServer<T> {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<HyperBody>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/raft.RaftServer/RequestVote" => {
                    struct RequestVoteSvc<T: RaftServer>(pub Arc<T>);
                    impl<T: RaftServer> tonic::server::UnaryService<super::RequestVoteRequest> for RequestVoteSvc<T> {
                        type Response = super::RequestVoteResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::RequestVoteRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.request_vote(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let method = RequestVoteSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec);
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/raft.RaftServer/AppendEntries" => {
                    struct AppendEntriesSvc<T: RaftServer>(pub Arc<T>);
                    impl<T: RaftServer> tonic::server::UnaryService<super::AppendEntriesRequest>
                        for AppendEntriesSvc<T>
                    {
                        type Response = super::AppendEntriesResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::AppendEntriesRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.append_entries(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let method = AppendEntriesSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec);
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/raft.RaftServer/ClientAppend" => {
                    struct ClientAppendSvc<T: RaftServer>(pub Arc<T>);
                    impl<T: RaftServer> tonic::server::UnaryService<super::ClientAppendRequest> for ClientAppendSvc<T> {
                        type Response = super::ClientAppendResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ClientAppendRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.client_append(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let method = ClientAppendSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec);
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/raft.RaftServer/ClientRequestIndex" => {
                    struct ClientRequestIndexSvc<T: RaftServer>(pub Arc<T>);
                    impl<T: RaftServer>
                        tonic::server::UnaryService<super::ClientRequestIndexRequest>
                        for ClientRequestIndexSvc<T>
                    {
                        type Response = super::ClientRequestIndexResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ClientRequestIndexRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.client_request_index(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let method = ClientRequestIndexSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec);
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: RaftServer> Clone for RaftServerServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: RaftServer> tonic::transport::ServiceName for RaftServerServer<T> {
        const NAME: &'static str = "raft.RaftServer";
    }
}
