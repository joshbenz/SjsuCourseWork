/*
    Joshua Benz
    008980257
    CS249 Raft
    This is my first time writing Rust, so hopefully it isn't too bad. It definately has a bit of
    a learning curve. I used Tonic (https://github.com/hyperium/tonic) which is the next version of Tower-RPC
    based on Tokio, Prost for protobuf, Tower and Hyper with Tokio async/await. I wanted to use a pure Rust implementation since I heard
    bad things about grpc-rust.
*/

use tonic::{transport::Server, 
    transport::Channel, 
    transport::Endpoint,
    Request, 
    Response, 
    Status};
use std::collections::{HashMap};
use std::io::prelude::*;
use std::fs;
use structopt::StructOpt;
use std::time::{Duration, Instant};
use tokio::prelude::*;
use std::sync::{Arc};
use tokio::sync::Mutex as Async_Mutex;
use std::thread;
use std::sync::mpsc;
use tokio::sync::watch;
use std::sync::atomic::{AtomicBool, AtomicUsize, AtomicU64, AtomicU32, AtomicU8, Ordering};
use std::collections::BTreeMap;
use std::convert::TryInto;


extern crate redis;
use redis::Commands;
extern crate rand;
use rand::Rng;



pub mod raft {
    tonic::include_proto!("raft"); 
}

use raft::{
    server::{RaftServer, RaftServerServer},
    client::{RaftServerClient},
    RequestVoteRequest, 
    RequestVoteResponse,
    AppendEntriesRequest, 
    AppendEntriesResponse,
    Entry, 
    ClientAppendRequest, 
    ClientAppendResponse,
    ClientRequestIndexRequest, 
    ClientRequestIndexResponse,
};


#[derive(Debug, StructOpt)]
#[structopt(
    name = "CS249 Raft",
    author = "Joshua Benz",
)]

struct Opt {
    infile: String,
    port: u32,
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum ServerState {
    FOLLOWER,
    CANDIDATE,
    LEADER,
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum TimerAction {
    RESET,
    ELECTION,
    HEARTBEAT,
}


pub struct MyRaftServer {
    state: Arc<RaftServerState>,
}

#[tonic::async_trait]
impl RaftServer for MyRaftServer {
    async fn request_vote(
        &self,
        request: Request<RequestVoteRequest>,
    ) -> Result<Response<RequestVoteResponse>, Status> {


        //take their term if higher????
        let vote_request = request.into_inner();
        println!("VoteRequest: ID: {} Term: {}",vote_request.candidate_id, vote_request.term);
        
        let mut current_term = self.state.state_manager.current_term();
        let voted_for = self.state.state_manager.voted_for();
        let commit_index = self.state.state_manager.commit_index();
        let latest_entry = self.state.state_manager.latest_entry().await;

        //If RPC request or response contains term T > currentTerm;
        //set currentTerm = T, convert to follower (5.1)
        if vote_request.term > current_term {
            current_term = vote_request.term;
            self.state.transition_state(ServerState::FOLLOWER, vote_request.candidate_id, current_term).await; //restarts timer automatically
        }


        //Receiver Implementation
        //1.) Reply false if term < currentTerm (5.1)
        if vote_request.term < current_term { 
            let reply = raft::RequestVoteResponse {
                term: current_term, 
                vote_granted: false
            };
            return Ok(Response::new(reply));
        }



        //2.) if votedFor is null or candidateId, and Candidate's log is
        //at least as upt-to-date as reciever's log, grant vote (5.2, 5.4)
        if (voted_for == 0 || voted_for == vote_request.candidate_id) 
            && (vote_request.last_log_index >= latest_entry.index) {
                //we have the same log index
                //check the term
                if vote_request.last_log_term >= latest_entry.term {
                    //vote granted
                    self.state.state_manager.voted_for.store(vote_request.candidate_id, Ordering::SeqCst);
                    let reply = raft::RequestVoteResponse {
                        term: vote_request.term, 
                        vote_granted: true
                    };

                    //Followers (5.2) No response from leaders or granting votes leads
                    //to timer timeout, so reset timer when granting a vote
                    &self.state.reset_timer().await;
                    return Ok(Response::new(reply));
                }

        }

        //defaut to false

        let reply = raft::RequestVoteResponse {
            term: current_term,//vote_request.term,
            vote_granted: false
        };

        Ok(Response::new(reply))
    }

    async fn append_entries(
        &self,
        request: Request<AppendEntriesRequest>,
    ) -> Result<Response<AppendEntriesResponse> ,Status> {
        
        let append_request = request.into_inner();

        let my_last_curr_status = self.state.state_manager.current_status();
        let is_electing = self.state.state_manager.is_electing();
        let mut my_latest_term = self.state.state_manager.current_term();


        //If RPC request or response contains term T > currentTerm;
        //set currentTerm = T, convert to follower (5.1)
        if append_request.term > my_latest_term {
            my_latest_term = append_request.term;
            self.state.transition_state(ServerState::FOLLOWER, append_request.leader_id, my_latest_term).await; //restarts timer automatically
        }


        let my_latest_leader = self.state.state_manager.leader_id();
        if my_latest_leader == append_request.leader_id {
            //we have heard from the leader, so reset timer
            &self.state.reset_timer().await;
        }

        //unfortunately prost doesnt make Entry impl the Copy trait, so 
        //matching performs a move on the value and I want to use it later. So I create
        //a reference and copy that instead.
        let entry_ref = append_request.entry.as_ref().map(|s| s.clone());

        //check for heartbeat
        match &entry_ref {
            //I'm not sure if this implementation of grpc will give me None
            //if someone else puts null for an empty entry, or if grpc will
            //just generate the 0's for entry. Also not sure if people are
            //sending null or if they even can, So i'll check for both
            Some(entry) => {
                if entry.index == 0 || entry.decree.is_empty() {
                    //this is a hearbeat
                    if my_latest_term <= append_request.term {
                        //Accept them as leader
                        println!("HB Leader {}, TERM: {}", append_request.leader_id, append_request.term);
                        &self.state.transition_state(ServerState::FOLLOWER, append_request.leader_id, append_request.term).await;
                    }

                    //no need to process further
                    let reply = raft::AppendEntriesResponse {
                        term: my_latest_term,
                        success: true,
                    };
                    return Ok(Response::new(reply));
                }
            }, 
            None => {
                //this is a hearbeat
                if  my_latest_term <= append_request.term {
                    //Accept them as leader
                    println!("HB Leader {} , TERM {}", append_request.leader_id, append_request.term);
                    &self.state.transition_state(ServerState::FOLLOWER, append_request.leader_id, append_request.term).await;
                }

                //no need to process further
                let reply = raft::AppendEntriesResponse {
                    term: my_latest_term,
                    success: true,
                };
                return Ok(Response::new(reply));
            }
        }

        

        println!("Append Entries: Term: {} Leader: {}", append_request.term, append_request.leader_id);
        //Receiver Implementation
        // 1.) Reply false if term < currentTerm (5.1)
        if append_request.term < my_latest_term {
            let reply = raft::AppendEntriesResponse {
                term: my_latest_term,
                success: false,
            };
            return Ok(Response::new(reply));
        }

        // 2.) Reply false if log doesn't contain an entry at prevLogIndex
        //whose term matches prevLogTerm (5.3)

        if !self.state.state_manager.contains_matching_entry_at(append_request.prev_log_index as usize, append_request.prev_log_term).await {
            let reply = raft::AppendEntriesResponse {
                term: my_latest_term,
                success: false,
            };
            return Ok(Response::new(reply));
        }


        match append_request.entry {
            Some(entry) => {
                // 3.) If an existing entry conflicts with a new one (same index
                // but different terms), delete the existing entry and all that follow it (5.3)
                if self.state.state_manager.contains_entry(entry.index as usize).await {
                    //a log exists here
                    if self.state.state_manager.conflicts(&entry).await {
                        //terms don't match, so they conflict, remove them
                        self.state.state_manager.drain_log(entry.index as usize).await;
                    }
                } 

                // 4.) append all new entries to the log
                self.state.state_manager.add_entry(Entry::clone(&entry)).await;

                //NOTE BROKE AFTER I CHANGED THIS
                // 5.) If leaderCommit > commitIndex, set commitIndex = 
                // min(leaderCommit, index of last new entry)
                if  append_request.leader_commit > self.state.state_manager.commit_index.load(Ordering::SeqCst) {
                    self.state.state_manager
                    .commit_index
                    .store(std::cmp::min(append_request.leader_commit, 
                        self.state.state_manager.last_entry_index.load(Ordering::SeqCst).try_into().unwrap()), Ordering::SeqCst);
                }
            },
            None => {
                //this shouldn't happen since it gets caught as a heatbeat
                //just fail if this happens
                let reply = raft::AppendEntriesResponse {
                    term: my_latest_term,
                    success: false,
                };
        
                return Ok(Response::new(reply));
            }
        }

        //if we made it this far, then we sucessfully added the log
        let reply = raft::AppendEntriesResponse {
            term: append_request.term,
            success: true,
        };

        Ok(Response::new(reply))
    }

    async fn client_append(
        &self,
        request: Request<ClientAppendRequest>,
    ) -> Result<Response<ClientAppendResponse>, Status> {

        if self.state.state_manager.current_status() == ServerState::LEADER as u8 {
            //only the leader can append entries from client
            let prev_entry = self.state.state_manager.latest_entry().await;
            let next_index = prev_entry.index + 1;

            //add entry and persist it
            let entry = Entry::new(self.state.state_manager.current_term(), next_index, request.into_inner().decree);
            self.state.state_manager.add_entry(Entry::clone(&entry)).await;

            //broadcast appendentries until majority persist the value
            let mut persisted: usize = 0;
            let stubs = self.state.state_manager.clients();
            let mut stubs_guard = stubs.lock().await;
            let mut success_map: HashMap<u32, bool> = HashMap::default(); //map of who replied alread

            while persisted < (stubs_guard.len()+1 /2) { //+1 since I don't save my stub
                for (id, stub) in stubs_guard.iter_mut() {
                    if !success_map.contains_key(id) {
                        let req = Request::new(AppendEntriesRequest { //this gets moved, so I have to create a new request each time
                            term: self.state.state_manager.current_term.load(Ordering::SeqCst),
                            leader_id: self.state.state_manager.id.load(Ordering::SeqCst),
                            prev_log_index: prev_entry.index,
                            prev_log_term: prev_entry.term,
                            entry: Some(Entry::clone(&entry)),
                            leader_commit: self.state.state_manager.commit_index(),
                        });
                    
                        match stub.append_entries(req).await {
                            Ok(res) => {
                                let res = res.into_inner();
                                if res.success {
                                    let mut match_index_guard = self.state.state_manager.match_index.lock().await;
                                    persisted += 1;
                                    
                                    match match_index_guard.insert(*id, entry.index as usize) {
                                        Some(k) => (),
                                        None => (),
                                    }

                                    match success_map.insert(*id, true) {
                                        Some(k) => (),
                                        None => (),
                                    }
                                //next index?????
                                }
                            },
                            Err(e) => (),
                        }
                    }
                }
                thread::sleep(Duration::from_millis(1000));
            }

            //If we ever get past this while loop, then it has persisted to the majority
            //and thus applied to the state machine
            let old = self.state.state_manager.last_applied.load(Ordering::SeqCst);
            if entry.index > old {
                self.state.state_manager.last_applied.store(entry.index, Ordering::SeqCst);
            }


            let reply = raft::ClientAppendResponse {
                rc: 0,
                leader: self.state.state_manager.leader_id() as u64,
                index: entry.index,
            };

            return Ok(Response::new(reply));
        }

        //I am not the leader
        let reply = raft::ClientAppendResponse {
            rc: 1,
            leader: self.state.state_manager.leader_id() as u64,
            index: 0,
        };

        Ok(Response::new(reply))
    }

    async fn client_request_index(
        &self,
        request: Request<ClientRequestIndexRequest>,
    ) -> Result<Response<ClientRequestIndexResponse>, Status> {

        let request = request.into_inner();
        println!("Client Request Index: {}", request.index);

        if self.state.state_manager.current_status() == ServerState::LEADER as u8 {
            let entry = self.state.state_manager.get(request.index as usize).await;

            let reply = raft::ClientRequestIndexResponse {
                rc: 0,
                leader: self.state.state_manager.leader_id() as u64,
                index: entry.index,
                decree: entry.decree,
            };

            return Ok(Response::new(reply));
        }

        //I am not the leader
        let reply = raft::ClientRequestIndexResponse {
            rc: 1,
            leader: self.state.state_manager.leader_id() as u64,
            index: 0,
            decree: "".to_string(),
        };

        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    //not expecting a huge file, so this should be fine
    let file_data = fs::read_to_string(opt.infile)
        .expect("Error while reading file");

    let s = Arc::new(RaftServerState::new(file_data, 9));
    s.state_manager.load_from_store().await;

    //just put this in manually
    let addr = String::from("192.168.43.9:")+&opt.port.to_string();
    let addr = addr.parse()?;
    let raft_server = MyRaftServer {
        state: Arc::clone(&s),
    };

    Server::builder()
        .add_service(RaftServerServer::new(raft_server))
        .serve(addr)
        .await?;

    Ok(())
}

/*
    RaftServerState is the main container for all the other structs. I pass it 
    into the Tonic server context to have access to all of this data and methods.
*/

struct RaftServerState {
    status_manager: Arc<Async_Mutex<StatusManager>>,
    state_manager: Arc<StateManager>,
}


impl RaftServerState {
    fn new(nodes: String, id: u32) -> Self {
        let mut conn_table = ClientConnectionTable::new(nodes);
        let state = Arc::new(StateManager::new(id, conn_table.client_ids_arc(), conn_table.conn_table_arc()));
        let elect = Arc::new(Async_Mutex::new(ElectoralManager::new(conn_table, Arc::clone(&state))));
        let status_mngr = Arc::new(Async_Mutex::new(StatusManager::new(Arc::clone(&elect), Arc::clone(&state))));

        return RaftServerState {
            status_manager: status_mngr,
            state_manager: state,
        }
    }

    //easy access for reseting the timer
    async fn reset_timer(&self) {
        let mut guard = self.status_manager.lock().await;
        guard.reset_timer();
    }

/*
    Transitions states and applies common tasks that go with that.
*/

    async fn transition_state(&self, status: ServerState, id: u32, term: u64) {
        self.state_manager.current_status.store(status as u8, Ordering::SeqCst);
        self.state_manager.current_term.store(term, Ordering::SeqCst);
        self.reset_timer().await;

        match status {
            ServerState::FOLLOWER => {
                self.state_manager.leader_id.store(id, Ordering::SeqCst);
                self.state_manager.is_electing.store(false, Ordering::SeqCst);
                self.state_manager.voted_for.store(0, Ordering::SeqCst); //I haven't voted fro anyone this term
            },
            ServerState::LEADER => {
                self.state_manager.leader_id.store(id, Ordering::SeqCst);
                self.state_manager.is_electing.store(false, Ordering::SeqCst);
                self.state_manager.reset_leader_state().await;
                println!("Look at me, I am the captain now...");
            },
            ServerState::CANDIDATE => {
                self.state_manager.leader_id.store(0, Ordering::SeqCst);
                self.state_manager.voted_for.store(id, Ordering::SeqCst);
                println!("Attempting to establish dominance..."); //as the paper would say
            }
        }
    }
}

/*
    StateManager holds Atomic Reference Counted references that gets passed around
    other places. 
*/

struct StateManager {
    current_term: Arc<AtomicU64>,
    //went with a BTreeMap to use it like an assoc array to make my life
    //easier when testing. With a Vector I was worried about the index used
    //to index into the log may not necessarily be the index of the entry.
    //a map garuntees that it is and a BTreeMap keeps things sorted for
    //removing entries after a specific index.
    log: Arc<Async_Mutex<BTreeMap<usize, Entry>>>, 
    commit_index: Arc<AtomicU64>,
    last_applied: Arc<AtomicU64>,
    voted_for: Arc<AtomicU32>,
    leader_id: Arc<AtomicU32>,
    id: Arc<AtomicU32>,
    current_status: Arc<AtomicU8>,
    is_electing: Arc<AtomicBool>,
    redis_client: Option<redis::Client>,
    last_entry_index: Arc<AtomicUsize>,
    next_index: Arc<Async_Mutex<HashMap<u32, usize>>>, //id and entry index
    match_index: Arc<Async_Mutex<HashMap<u32, usize>>>,
    client_ids: Arc<Async_Mutex<Vec<u32>>>, //by clients I mean other servers. in Go they called stubs clients,
    clients: Arc<Async_Mutex<HashMap<u32, RaftServerClient<Channel>>>>,
}


impl StateManager {
    fn new(id: u32, client_ids: Arc<Async_Mutex<Vec<u32>>>, clients: Arc<Async_Mutex<HashMap<u32, RaftServerClient<Channel>>>>) -> Self {
        match redis::Client::open("redis://127.0.0.1/") {
            Ok(c) => {
                let s = StateManager {
                    current_term: Arc::new(AtomicU64::new(0)),
                    commit_index: Arc::new(AtomicU64::new(0)),
                    last_applied: Arc::new(AtomicU64::new(0)),
                    voted_for: Arc::new(AtomicU32::new(0)),
                    leader_id: Arc::new(AtomicU32::new(0)),
                    log: Arc::new(Async_Mutex::new(BTreeMap::new())),
                    id: Arc::new(AtomicU32::new(id)),
                    current_status: Arc::new(AtomicU8::new(ServerState::FOLLOWER as u8)),
                    is_electing: Arc::new(AtomicBool::new(false)),
                    last_entry_index: Arc::new(AtomicUsize::new(0)),
                    redis_client: Some(c),
                    next_index: Arc::new(Async_Mutex::new(HashMap::default())),
                    match_index: Arc::new(Async_Mutex::new(HashMap::default())),
                    client_ids: client_ids,
                    clients: clients,
                };
                return s;
            },
            Err(e) => panic!(e),
        }
        
        StateManager {
            current_term: Arc::new(AtomicU64::new(0)),
            commit_index: Arc::new(AtomicU64::new(0)),
            last_applied: Arc::new(AtomicU64::new(0)),
            voted_for: Arc::new(AtomicU32::new(0)),
            leader_id: Arc::new(AtomicU32::new(0)),
            log: Arc::new(Async_Mutex::new(BTreeMap::new())),
            id: Arc::new(AtomicU32::new(id)),
            current_status: Arc::new(AtomicU8::new(ServerState::FOLLOWER as u8)),
            is_electing: Arc::new(AtomicBool::new(false)),
            last_entry_index: Arc::new(AtomicUsize::new(0)),
            redis_client: None,
            next_index: Arc::new(Async_Mutex::new(HashMap::default())),
            match_index: Arc::new(Async_Mutex::new(HashMap::default())),
            client_ids: client_ids,
            clients: clients,
        }
    }

    //return raw copies of values from the atomic values
    fn current_term(&self) -> u64 {
        return self.current_term.load(Ordering::SeqCst);
    }

    fn voted_for(&self) -> u32 {
        return self.voted_for.load(Ordering::SeqCst);
    }

    fn commit_index(&self) -> u64 {
        return self.commit_index.load(Ordering::SeqCst);
    }

    fn leader_id(&self) -> u32 {
        return self.leader_id.load(Ordering::SeqCst);
    }

    fn current_status(&self) -> u8 {
        return self.current_status.load(Ordering::SeqCst);
    }

    fn is_electing(&self) -> bool {
        return self.is_electing.load(Ordering::SeqCst);
    }

    fn clients(&self) -> Arc<Async_Mutex<HashMap<u32, RaftServerClient<Channel>>>> {
        return Arc::clone(&self.clients);
    }




    //returns a copy of the latest entry
    async fn latest_entry(&self) -> Entry {
        let guard = self.log.lock().await;
        if guard.is_empty() {
            return Entry::empty_entry(); 
        } else {
            match guard.get(&self.last_entry_index.load(Ordering::SeqCst)) {
                Some(entry) => {
                    return Entry::clone(&entry);
                },
                None => {
                    return Entry::empty_entry();
                },
            }
        }
    }

    //returns entry at that index, or the latest entry
    async fn get(&self, index: usize) -> Entry {
        let guard = self.log.lock().await;
        if guard.is_empty() {
            return Entry::empty_entry(); 
        } else {
            match guard.get(&index) {
                Some(entry) => {
                    return Entry::clone(&entry);
                },
                None => {
                    drop(guard);
                    return self.latest_entry().await;        
                },
            }
        }
    }

    //true if a log's entry term doesn't match the specified entry's term
    async fn conflicts(&self, entry: &Entry) -> bool {
        let guard = self.log.lock().await;

        match guard.get(&(entry.index as usize)) {
            Some(log_entry) => {
                if log_entry.term != entry.term {
                    return true;
                }
            },
            None => {
                return false;
            },
        }
        return false;
    }

    //adds an entry to log in memory and then persists it to redis store
    //also updates commitIndex and lastApplied
    async fn add_entry(&self, entry: Entry) {
        let mut guard = self.log.lock().await;
        let mut index = entry.index as usize;
        match guard.insert(index, Entry::clone(&entry)) {
            Some(old_entry) => (), //value updated
            None => (), //this was new instert
        }
        drop(guard);
        self.last_entry_index.store(entry.index as usize, Ordering::SeqCst);

        match &self.redis_client {
            Some(redis) => { //impl std::Future for async and shared async conns
                match redis.get_connection() { 
                    Ok(mut conn) => {
                        match conn.set(entry.index.to_string(), entry.to_redis()) {
                            Ok(()) => {
                                //entry has been applied to local log
                                //and committed to state machine, but not yet
                                //applied to state machine
                                self.commit_index.store(entry.index, Ordering::SeqCst);
                            }, 
                            Err(e) => (),
                        }
                    },
                    Err(e) => println!("Redis Connection err {}", e),
                }
            },
            None => {
                println!("Something went wrong with redis client");
            }
        }
        self.print_persisted_log();
    }

    //returns false if no matching log exists or if the log doesnt exist in general
    async fn contains_matching_entry_at(&self, prev_log_index: usize, prev_log_term: u64) -> bool{
        let mut guard = self.log.lock().await;

        if guard.is_empty() {
            return true; //there's nothing to match to
        }

        match guard.get(&prev_log_index) {
            Some(entry) => { //we have a log there
                if entry.term != prev_log_term {
                    return false; //there is a term conflict
                } else { //there is no conflict
                    return true;
                }
            },
            None => {
                //log doesn't exist at that index
                return false;
            },
        }
    }

    //true if the log contains an entry at the specified index and the index in the entry matches
    async fn contains_entry(&self, index: usize) -> bool {
        let mut guard = self.log.lock().await;
        return guard.contains_key(&index);
    }

    //removes entries from [index, end] and updates persistant storage with changes
    async fn drain_log(&self, index: usize) {
        let mut guard = self.log.lock().await;
        //splits the BTreeMap into a two a new one
        let removed = guard.split_off(&index);

        match &self.redis_client {
            Some(redis) => { //impl std::Future for async and shared async conns
                match redis.get_connection() { 
                    Ok(mut conn) => {
                        let keys: Vec<_> = removed.keys().cloned().collect();
                        for key in keys {
                            match conn.del(key) {
                                Ok(()) => (),
                                Err(e) => (),
                            }
                        }
                    },
                    Err(e) => println!("Redis Connection err {}", e),
                }
            },
            None => {
                println!("Something went wrong with redis client");
            }
        }
        self.print_persisted_log();
    }

/*
    Prints the log[] straight from persisted storage (Redis)
*/

    fn print_persisted_log(&self) {
        match &self.redis_client {
            Some(redis) => { //impl std::Future for async and shared async conns
                match redis.get_connection() { 
                    Ok(mut conn) => {
                        let mut iter : redis::Iter<isize> = redis::cmd("SCAN")
                            .cursor_arg(0).clone().iter(&mut conn).unwrap();

                            let mut vec:Vec<u64> = Vec::new();
                        for x in iter {
                            vec.push(x as u64);
                        }

                        let mut result = String::from("");
                        for index in vec {
                            let s:String = conn.get(index).unwrap();
                            let mut entry = Entry::from_redis(s);
                            result.push_str(&entry.to_string());
                        }
                        println!("Persisted Log\n{}", result);
                        
                    },
                    Err(e) => println!("Redis Connection err {}", e),
                }
            },
            None => {
                println!("Something went wrong with redis client");
            }
        }
    }

    /*
        Loads persisted data from Redis into program memory. This includes
        currentTerm, votedFor and log[]
    */

    async fn load_from_store(&self) {
        match &self.redis_client {
            Some(redis) => { //impl std::Future for async and shared async conns
                match redis.get_connection() { 
                    Ok(mut conn) => {
                        let mut iter : redis::Iter<isize> = redis::cmd("SCAN")
                            .cursor_arg(0).clone().iter(&mut conn).unwrap();

                        let mut vec:Vec<u64> = Vec::new();
                        for x in iter {
                            if x.to_string() != "currentTerm" && x.to_string() != "votedFor" {
                                vec.push(x as u64);
                            }
                        }

                        if vec.len() == 0 {
                            //nothing to load
                            return;
                        }

                        for index in vec {
                            let s:String = conn.get(index).unwrap();   
                            let entry = Entry::from_redis(s);
                            self.add_entry(entry).await;
                        }

                        let mut voted_for:u32 = conn.get("votedFor").unwrap();
                        let mut current_term:u64 = conn.get("currentTerm").unwrap();

                        self.voted_for.store(voted_for, Ordering::SeqCst);
                        self.current_term.store(current_term, Ordering::SeqCst);
                        println!("Persisted Term: {}\nPersisted VotedFor: {}", current_term, voted_for);
                        self.print_local_store().await;

                    },
                    Err(e) => println!("Redis Connection err {}", e),
                }
            },
            None => {
                println!("Something went wrong with redis client");
            }
        }
    }

    /*
        Prints the log data that I have in program memory
    */
    async fn print_local_store(&self) {
        let mut guard = self.log.lock().await;

        let mut result = String::from("");
        for (id, entry) in guard.iter_mut() {
            result.push_str(&entry.to_string());
        }
        println!("Log In Memory\n{}", result);
    }


    /*
        Whenever there is a new election, this is called to reset the nexIndex[]
        and matchIndex[]. Although it is reset, the memory is kept around to be
        reused.
    */

    async fn reset_leader_state(&self) {
        let mut guard = self.next_index.lock().await;
         //returns everything from the hashmap, 
         //which empties it but keeps the memory allocated for later use
        let drain = guard.drain();
        drop(drain); //remove mutable borrow

        let mut match_guard = self.match_index.lock().await;
        let drain = match_guard.drain();
        drop(drain); //remove mutable borrow

        let mut ids_guard = self.client_ids.lock().await;

        let last_log_index = self.last_entry_index.load(Ordering::SeqCst);
        for id in ids_guard.iter() {
            match match_guard.insert(*id, 0 as usize) {
                Some(r) => (),
                None => (),
            }

            match guard.insert(*id, last_log_index+1) {
                Some(r) => (),
                None => (),
            }
        }
    }

    //Not yet implemented
    fn monitor_leader_state(&self) {
        let current_status_ref = Arc::clone(&self.current_status);
        let next_index_ref = Arc::clone(&self.next_index);
        let match_index_ref = Arc::clone(&self.match_index);
        
        tokio::spawn(async move {
            loop {
                if current_status_ref.load(Ordering::SeqCst) == ServerState::LEADER as u8 {
                    let next_index_guard = next_index_ref.lock().await;
                    let match_index_guard = match_index_ref.lock().await;
                    //TODO: if nextIndex != matchIndex..try to send that entry again.
                    //or somethng
                }

                thread::sleep(Duration::from_millis(1000));
            }
        });
    }

    fn persist_term_and_voted_for(&self) {
        match &self.redis_client {
            Some(redis) => { 
                match redis.get_connection() { 
                    Ok(mut conn) => {
                        //persist currentTerm
                        match conn.set("currentTerm", self.current_term.load(Ordering::SeqCst).to_string()) {
                            Ok(()) => (), 
                            Err(e) => (),
                        }

                        //persist votedFor
                        match conn.set("votedFor", self.voted_for.load(Ordering::SeqCst).to_string()) {
                            Ok(()) => (), 
                            Err(e) => (),
                        }
                    },
                    Err(e) => println!("Redis Connection err {}", e),
                }
            },
            None => {
                println!("Something went wrong with redis client");
            }
        }
    }
}


/*
    ElectoralManager deals with triggering, broadcasting and cancelling elections
*/

struct ElectoralManager {
    is_electing: Arc<AtomicBool>, //true if i am in the election process
    connection_table: Arc<Async_Mutex<ClientConnectionTable>>,
    state_ref: Arc<StateManager>
}

impl ElectoralManager {
    fn new(conn: ClientConnectionTable, state: Arc<StateManager>) -> Self {
        let e = ElectoralManager {
            is_electing: Arc::clone(&state.is_electing),
            connection_table: Arc::new(Async_Mutex::new(conn)),
            state_ref: state,
        };
        return e;
    }


    /*
        Runs elections until someone has become the leader.
    */

   async fn run_election(&mut self) {
       self.is_electing.store(true, Ordering::SeqCst);
       self.state_ref.voted_for.store(self.state_ref.id.load(Ordering::SeqCst), Ordering::SeqCst); //voted for myself

       while self.is_electing.load(Ordering::SeqCst) {
        println!("ELECTION CALLED, TERM: {}",  self.state_ref.current_term.load(Ordering::SeqCst)+1);
            let time = Duration::from_secs(ElectoralManager::gen_election_time().into());
            let fut = ElectoralManager::broadcast_votes(
                Arc::clone(&self.connection_table),
                Arc::clone(&self.state_ref)).timeout(time).await;

            thread::sleep(time + Duration::from_millis(5)); //just to be sure. timeout() is supposed to accurate to he milisecond

            match fut {
                Ok(success) => {
                    println!("ELECTION: {} TERM: {}", success, self.state_ref.current_term.load(Ordering::SeqCst));
                    if success {
                        self.is_electing.store(false, Ordering::SeqCst);
                        self.state_ref.leader_id.store(self.state_ref.id.load(Ordering::SeqCst), Ordering::SeqCst);
                        self.state_ref.current_status.store(ServerState::LEADER as u8, Ordering::SeqCst);
                        self.state_ref.reset_leader_state().await;
                        return;
                    }
                },
                Err(e) => {
                    //election timed out
                },
            }
       }
    }


    /*
        broadcasts votes to all servers
    */

       async fn broadcast_votes (
        connection_table: Arc<Async_Mutex<ClientConnectionTable>>,
        state_ref: Arc<StateManager>,
    ) -> bool {

        state_ref.current_term.fetch_add(1, Ordering::SeqCst);
            let mut votes: usize = 1; //vote for myself

            let mut guard = connection_table.lock().await;
            guard.try_dial().await;
            let conn_routing = guard.conn_table_arc(); //get a referece to client stubs
            drop(guard);

            let mut guard_clients = conn_routing.lock().await;
            let mut clients = guard_clients.values().cloned().collect::<Vec<_>>();
            let mut n_clients = clients.len(); //keep track of responses
            let mut size = clients.len(); //return the size. Clients gets moved so I can't access it after the for loop

            for client in clients {
                if !state_ref.is_electing.load(Ordering::SeqCst) {
                    return false; //stop the election
                }

                let latest_entry = state_ref.latest_entry().await;
                let mut client = Arc::new(client);
                let request = Request::new(RequestVoteRequest { //this gets moved, so I have to create a new request each time
                    term: state_ref.current_term.load(Ordering::SeqCst),
                    candidate_id: state_ref.id.load(Ordering::SeqCst),
                    last_log_index: latest_entry.index,
                    last_log_term: latest_entry.term,
                });
        

                match Arc::get_mut(&mut client).unwrap().request_vote(request).await {
                    Ok(response) => {
                        let res = response.into_inner();
                        
                        //If RPC request or response contains term T > currentTerm
                        //set currentTerm = T, convert to follower (5.1)
                        if res.term > state_ref.current_term.load(Ordering::SeqCst) {
                            state_ref.current_term.store(res.term, Ordering::SeqCst); //update my term
                            state_ref.is_electing.store(false, Ordering::SeqCst);
                            state_ref.current_status.store(ServerState::FOLLOWER as u8, Ordering::SeqCst);
                            state_ref.voted_for.store(0, Ordering::SeqCst); //I haven't voted for anyone this term
                            return false;
                        }
                        n_clients -= 1; //how many responses I expect to see
                        if res.vote_granted {
                            votes += 1;
                        }
                    },
                    Err(e) => (),
                }  
            }  
            drop(guard_clients);

            while n_clients != 0 {
                //spin the hamster weels. not ideal
            }
            println!("VOTES GOT: {} of {}", votes, size+1);
            return ElectoralManager::is_majority(votes, size+1);
    }


    /*
        Checks if I have the majority votes
    */

    fn is_majority(votes: usize, nodes: usize) -> bool {
        if nodes < 2 { //The file is empty, or I am the file
            return true;
        } 

        if nodes == 2 { //if there is one other person, I'll just say I need both votes
            if votes == nodes { //although this case wouln't work out since we vote for ourselves + someone else
                return true;    // we would both end up with two votes probably. Bank on the random timeouts
            }
        } else {
            //now I care how many
            if votes > (nodes /2) {
                return true;
            }
        }
        return false;
    }


    /*
        Generates a random election timout between 1 and 5
    */

    fn gen_election_time() -> u32 {
        let mut rng = rand::thread_rng();
        let time: u32 = rng.gen_range(1, Timer::ELECTION_TIMEOUT_SECONDS+1);
        println!("Generating Election time: {} seconds", time);
        time
    }


    /*
        Spawns an async task to broadcast heartbeats to all servers. Note that 
        nodes are only redialed if they failed to dial in previous iterations.
    */

    fn heartbeat(&mut self) {
        ElectoralManager::broadcast_heartbeats(
            Arc::clone(&self.connection_table),
            Arc::clone(&self.state_ref),
        );
    }

    fn broadcast_heartbeats(connection_table: Arc<Async_Mutex<ClientConnectionTable>>,
        state_ref: Arc<StateManager>) {

        tokio::spawn(async move {
            let mut guard = connection_table.lock().await;
            guard.try_dial().await;
            let conn_routing = guard.conn_table_arc(); //get a referece to client stubs
            drop(guard);

            let mut guard_clients = conn_routing.lock().await;
            let mut clients = guard_clients.keys().cloned().collect::<Vec<_>>();
            
            for id in clients { 
                let mut client = Arc::new(guard_clients.get_mut(&id).unwrap());
                let request = Request::new(AppendEntriesRequest { //this gets moved, so I have to create a new request each time
                    term: state_ref.current_term.load(Ordering::SeqCst),
                    leader_id: state_ref.id.load(Ordering::SeqCst),
                    prev_log_index: 0 as u64,
                    prev_log_term: 0 as u64,
                    entry: None,
                    leader_commit: 0 as u64,
                });            
                    
                match Arc::get_mut(&mut client).unwrap().append_entries(request).await {
                    Ok(response) => {
                        let res_term = response.into_inner().term;
                        //If RPC request or response contains term T > currentTerm
                        //set currentTerm = T, convert to follower (5.1)
                       if res_term > state_ref.current_term.load(Ordering::SeqCst) {
                            state_ref.current_term.store(res_term, Ordering::SeqCst); //update my term
                            state_ref.is_electing.store(false, Ordering::SeqCst);
                            state_ref.current_status.store(ServerState::FOLLOWER as u8, Ordering::SeqCst);
                            state_ref.voted_for.store(0, Ordering::SeqCst); //I haven't voted for anyone this term
                            return;
                        } 
                    },
                    Err(e) => {
                        //For some reason once the stubs get disconnected, it won't try to reconnect
                        //so i'l just remove the stub so that the next time it will try again and keep 
                        //trying until it connects.
                        println!("something went wrong: {:?}", e);
                        guard_clients.remove(&id).unwrap();
                        println!("Removing Stub");
                    },
                }
            }
        });
    }
}

/*
   StatusManager deals with the status of the server, aka Leader, Candidate, Follower.
   The monitors in here are responsible for receiving msgs from the timer when 
   certain timer periods have elapsed, Processing those msgs and triggering events
   such as running and election depending on my current status. 
*/

struct StatusManager {
    electoral_manager: Arc<Async_Mutex<ElectoralManager>>,
    timer: Timer, //for the sake of my sanity
    state_ref: Arc<StateManager>,

}

impl StatusManager {
    fn new(electoral_manager: Arc<Async_Mutex<ElectoralManager>>, state_ref: Arc<StateManager>) -> Self {
        let mut s = StatusManager {
            electoral_manager: electoral_manager,
            state_ref: state_ref,
            timer: Timer::new(),
        };
        s.start_timer();
        return s;
    }

/*
    Driver that start the async tasks with the needed information
*/
    fn start_timer(&mut self) {
        let rx = self.timer.start();
        StatusManager::monitor_timer(
            rx,
            Arc::clone(&self.electoral_manager),
            Arc::clone(&self.state_ref.current_status),
            Arc::clone(&self.state_ref),
            self.timer.sender.clone(),
        );
    }

    fn reset_timer(&mut self) {
        self.timer.reset();
    }

/*
    Checks for msgs that the Timer will broadcast when certian time periods have
    elapsed. I also spawn another async task here to listen for state changes
    to current_term, voted_for and current_status.
*/

    fn monitor_timer(mut rx: watch::Receiver<TimerAction>,
        electoral_manager: Arc<Async_Mutex<ElectoralManager>>,
        current_status: Arc<AtomicU8>,
        state_ref: Arc<StateManager>,
        mut timer_tx: mpsc::Sender<TimerAction>) {

        let arc_state_ref = Arc::clone(&state_ref);
        tokio::spawn(async move {
            loop {

                while let Some(action) = rx.recv().await {
                    println!("STATUS: {} TERM: {}",  current_status.load(Ordering::SeqCst), state_ref.current_term.load(Ordering::SeqCst));
                    match action {
                        TimerAction::HEARTBEAT => {
                            //I only care about this timer if I am a Leader
                            if current_status.load(Ordering::SeqCst) == ServerState::LEADER as u8 {
                                //broadcast heartbeats
                                let mut guard = electoral_manager.lock().await;
                                guard.heartbeat();
                                drop(guard);
                            }
                        },
                        TimerAction::ELECTION => {
                            if current_status.load(Ordering::SeqCst) == ServerState::FOLLOWER as u8 {
                                current_status.store(ServerState::CANDIDATE as u8, Ordering::SeqCst);
                                let mut guard = electoral_manager.lock().await;
                                guard.run_election().await;
                                drop(guard);
                            }
                        },
                        TimerAction::RESET => (),
                    }
                }
            }
        });

        //monitor status transitions in a very terrible manner
        tokio::spawn(async move {
            timer_tx = timer_tx.clone();
            //monitor state changes
            //should have made better design choices lol
            loop {
                let last_known_status = arc_state_ref.current_status.load(Ordering::SeqCst);
                let last_known_voted_for = arc_state_ref.voted_for.load(Ordering::SeqCst);
                let last_known_current_term = arc_state_ref.current_term.load(Ordering::SeqCst);
                thread::sleep(Duration::from_millis(100));

                if last_known_status != ServerState::FOLLOWER as u8 {
                    if arc_state_ref.current_status.load(Ordering::SeqCst) == ServerState::FOLLOWER as u8 {
                        //every time i transition to FOLLOWER, reset the timer
                        match timer_tx.send(TimerAction::RESET) {
                            Err(e) => println!("Error Timer Reset: {}", e),
                            Ok(_) => (),
                        };
                    }
                }
                
                if last_known_voted_for != arc_state_ref.voted_for.load(Ordering::SeqCst) ||
                    last_known_current_term != arc_state_ref.current_term.load(Ordering::SeqCst) {
                        arc_state_ref.persist_term_and_voted_for();
                }
            }

        });
    }
}

/*
    Timer deals with checking for Election and Heartbeat timouts in an Async Task.
    Once elapsed, msgs are broadcasted to other tasks.
*/

struct Timer {
    sender: mpsc::Sender<TimerAction>,
}


impl Timer {
    const HEARTBEAT_TIMOUT_SECONDS: u32 = 10;
    const ELECTION_TIMEOUT_SECONDS: u32 = 5;
    const HEARTBEAT_INTERVAL: u32 = 3;

    fn new() ->  Self {
        let (sender, receiver) = mpsc::channel(); 
        return Timer {
            sender: sender,
        }
    }

    /*
        Driver for to start the infinite timer
    */

    fn start(&mut self) -> watch::Receiver<TimerAction> {
        let (timer_send, mut timer_rcv) = mpsc::channel();
        let (mut tx, mut rx) = watch::channel(TimerAction::RESET);

        self.sender = timer_send;
        Timer::start_timer(timer_rcv, tx);
        return rx;
    }

    /*
        Starts and infinite timer and checks for how much time has elapsed since
        a particular instant. Reseting the timer updates the instant. A channel
        is used to communicate commands to other tasks. 
        Broadcasts when the election timeout and heartbeat timeout has elapsed
    */
    fn start_timer(mut timer_rcv: mpsc::Receiver<TimerAction>, tx: watch::Sender<TimerAction>) {

        tokio::spawn(async move {
            let mut time = Instant::now();
            let mut heartbeat_time = Instant::now();
            loop {

                match timer_rcv.try_recv() {
                    Ok(action) => {
                        match action {
                            TimerAction::RESET => {
                                time = Instant::now();
                            },
                            TimerAction::ELECTION => (),
                            TimerAction::HEARTBEAT => (),
                        }
                    },
                    Err(e) => (), //nothing in the channel
                }

                if heartbeat_time.elapsed() >= Duration::from_secs(Timer::HEARTBEAT_INTERVAL.into()) {
                    heartbeat_time = Instant::now();
                    tx.broadcast(TimerAction::HEARTBEAT).unwrap();
                }
    
                if time.elapsed() >= Duration::from_secs(Timer::HEARTBEAT_TIMOUT_SECONDS.into()) {
                    time = Instant::now();
                    tx.broadcast(TimerAction::ELECTION).unwrap();
                }
            }
        });
    }

    /*
        sends a msg through a channel to one of my async tasks to restart the timer
    */
    fn reset(&mut self) {
        match self.sender.send(TimerAction::RESET) {
            Err(e) => println!("Error Timer Reset: {}", e),
            Ok(_) => (),
        };
    }
}

/*
    ClientConnectionTable contains all the necessry data to connect to clients
    and keeps track of client stubs
*/

struct ClientConnectionTable {
    node_routing: Arc<Async_Mutex<HashMap<u32, RaftServerClient<Channel>>>>,
    node_file_data: String,
    connected: Arc<AtomicBool>, //when connections to all nodes have been established
    n_nodes: Arc<AtomicUsize>,
    client_ids: Arc<Async_Mutex<Vec<u32>>>,
}

impl ClientConnectionTable {

    fn new(data: String) -> Self {
        ClientConnectionTable {
            node_routing: Arc::new(Async_Mutex::new(HashMap::default())),
            node_file_data: data,
            connected: Arc::new(AtomicBool::new(false)),
            n_nodes: Arc::new(AtomicUsize::new(0)),
            client_ids: Arc::new(Async_Mutex::new(Vec::new())),
        }
    }

    /*
        Driver for dial_disconnected_nodes
    */
    async fn try_dial(&mut self) {
        if self.connected.load(Ordering::SeqCst) {
            return;
        }
        let copy = self.node_file_data.clone();
        let lines: Vec<String> = copy.lines().map(|i| i.to_string()).collect();
        ClientConnectionTable::dial_disconnected_nodes(Arc::clone(&self.node_routing), 
            lines, 
            Arc::clone(&self.connected),
            Arc::clone(&self.n_nodes),
            Arc::clone(&self.client_ids)).await;
    }

    /*
        Atomically counted References I use in other place
    */
    fn conn_table_arc(&mut self) -> Arc<Async_Mutex<HashMap<u32, RaftServerClient<Channel>>>> {
        return Arc::clone(&self.node_routing);
    }

    fn client_ids_arc(&mut self) -> Arc<Async_Mutex<Vec<u32>>> {
        return Arc::clone(&self.client_ids)
    }


    /*
        Only creates stubs for ones that failed in the past.
    */
    async fn dial_disconnected_nodes(routing_table: Arc<Async_Mutex<HashMap<u32, RaftServerClient<Channel>>>>,
        conn_data: Vec<String>,
        connected: Arc<AtomicBool>,
        n_nodes: Arc<AtomicUsize>,
        client_ids: Arc<Async_Mutex<Vec<u32>>>) {
            let mut client_ids_guard = client_ids.lock().await;
            client_ids_guard.clear();
        
            let mut count: usize = 0; 
            let mut guard = routing_table.lock().await;
            for conn in conn_data {
                count += 1;
                let mut iter = conn.split_whitespace();
                let id: u32 = iter.next().unwrap().parse().unwrap();
                let addr = String::from(iter.next().unwrap());
                client_ids_guard.push(id);
        
                if !guard.contains_key(&id) {
                    match RaftServerClient::connect("http://".to_string() + &addr).await {
                        Ok(mut stub) => {
                            println!("Adding {:?}, {:?}", id, addr);
                            guard.insert(id, stub);
                        },
                        Err(e) => println!("Failed to connect to: {:?}, {:?}:::{:?}", id, addr, e),
                    }
                } //otherwise we already have a connection
            }

            if guard.len() ==  count {
                connected.store(true, Ordering::SeqCst);
            } // else some nodes were not operational at this time
            n_nodes.store(guard.len(), Ordering::SeqCst);
    }

}


//Using the prost generated struct
impl Entry {
    fn new(term: u64, index: u64, decree: String) -> Self {
        Entry {
            term,
            index,
            decree,
        }
    }
    fn empty_entry() -> Self {
        Entry {
            term: 0,
            index: 0,
            decree: String::from(""),
        }
    }

    fn clone(entry: &Entry) -> Self {
        Entry {
            term: entry.term,
            index: entry.index,
            decree: entry.decree.clone(),
        }
    }

    fn to_redis(&self) -> String {
        let mut result = self.term.to_string();
        result.push_str(":");
        result.push_str(&self.index.to_string());
        result.push_str(":");
        result.push_str(&self.decree);
        return result;
    }

    fn from_redis(data: String) -> Entry {
        let v: Vec<&str> = data.split(":").collect();
        return Entry::new(
            v[0].parse::<u64>().unwrap(),
            v[1].parse::<u64>().unwrap(),
            v[2].to_string()
        );
    }

    fn to_string(&mut self) -> String{
        return format!("Term: {} Index: {} Decree: {}\n", self.term, self.index, self.decree);
    }
}