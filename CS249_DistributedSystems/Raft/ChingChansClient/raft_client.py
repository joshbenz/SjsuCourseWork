import grpc

import raft_pb2
import raft_pb2_grpc

if __name__ == "__main__":
    conf = open("./raft_conf.conf", 'r')
    lines = conf.readlines()

    table = {}
    for line in lines:
        contents = line.split(' ')
        table[contents[0]] = {"ip:port": contents[1].split('\n')[0], 'stub': None}
    conf.close()

    leader = -1
    for key in table:
        while True:
            try:
                channel = grpc.insecure_channel(table[key]['ip:port'])
                stub = raft_pb2_grpc.RaftServerStub(channel)
                table[key]['stub'] = stub
                print("creating stub to {} successes".format(table[key]['ip:port']))
                break
            except Exception as e:
                print(e)
                print("createing stub to {} fails".format(table[key]['ip:port']))

        leader = str(key)

    while True:
        org = input("Input Command:")
        cmd = org.split(' ')
        if len(cmd) != 2:
            print("Invalid Command: {}".format(org))
            continue

        if cmd[0] == 'add':
            rsp = table[leader]['stub'].ClientAppend(raft_pb2.ClientAppendRequest(decree=cmd[1]))
            while rsp.rc != 0:
                leader = str(rsp.leader)
                print('Leader is changed to {}'.format(leader))
                rsp = table[leader]['stub'].ClientAppend(raft_pb2.ClientAppendRequest(decree=cmd[1]))

            print("Request OK with Index:{}".format(rsp.index))
        elif cmd[0] == 'get':
            rsp = table[leader]['stub'].ClientRequestIndex(raft_pb2.ClientRequestIndexRequest(index=int(cmd[1])))
            while rsp.rc != 0:
                leader = str(rsp.leader)
                print('Leader is changed to {}'.format(leader))
                rsp = table[leader]['stub'].ClientRequestIndex(raft_pb2.ClientRequestIndexRequest(index=int(cmd[1])))

            print("Request OK value is {} at index:{}".format(rsp.decree, rsp.index))
        else:
            print("Invalid Command: {}".format(org))
            continue