package main

import (
	"context"
	"log"
	"time"

	pb "./api"
	empty "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
)

const (
	address     = "localhost:1234"
	defaultName = "world"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewABDServiceClient(conn)

	// Contact the server and print out its response.

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var isempty *empty.Empty = new(empty.Empty)
	r, err := c.Name(ctx, isempty)
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", r.GetName())

	resWrite, err := c.Write(ctx, &pb.WriteRequest{
		Register:   "1",
		Timestampe: (uint64)(time.Now().UnixNano()),
		Value:      "myValue"})

	log.Printf("Greeting: %s", resWrite)
	/*
		resRead, err := c.Read1(ctx, &pb.Read1Request{Register: "1"})

		log.Printf("Greeting: %s", resRead)

		resRead2, err := c.Read2(ctx, &pb.Read2Request{Register: "1",
			Timestamp: (uint64)(time.Now().UnixNano()),
			Value:     "3"})

		log.Printf("Greeting: %s", resRead2)

		resRead, err = c.Read1(ctx, &pb.Read1Request{Register: "1"})

		log.Printf("Greeting: %s", resRead)*/

}
