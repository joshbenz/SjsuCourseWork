package main

import (
	"context"
	"log"
	"net"
	"os"
	"strconv"
	"strings"

	pb "./api"

	"github.com/go-redis/redis"
	empty "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
)

type server struct{}

var redisClient *redis.Client

func (s *server) Name(ctx context.Context, empty *empty.Empty) (*pb.NameResponse, error) {
	return &pb.NameResponse{Name: "Joshua Benz Server"}, nil
}

func (s *server) Read1(ctx context.Context, in *pb.Read1Request) (*pb.Read1Response, error) {
	//get the most recent value and return it
	log.Printf("Read1Request Register: " + in.GetRegister())

	//if no val, we will send this
	var val string = ""
	var time uint64 = 0

	reg := in.GetRegister()
	redisVal, redisErr := redisClient.Get(reg).Result()
	if redisErr == redis.Nil {
		//No value
	} else if redisErr != nil {
		panic(redisErr)
	} else {
		//return the latest
		val, time := parseRedisVal(redisVal)
		log.Printf("Sending Read1Response " + redisVal)
		return &pb.Read1Response{Timestamp: time, Value: val}, nil
	}
	//return the defaut

	log.Printf("NO VALUE, Sending Read1Response " + val + ":" + strconv.FormatUint(time, 10))
	return &pb.Read1Response{Timestamp: time, Value: val}, nil
}

func (s *server) Read2(ctx context.Context, in *pb.Read2Request) (*pb.AckResponse, error) {
	//reader has adopted its value
	//this is the announcement of the value it adopted
	log.Printf("Read2Request Register: " + in.GetRegister() + " Value: " + in.GetValue() + " Time: " + strconv.FormatUint(in.GetTimestamp(), 10))
	time := strconv.FormatUint(in.GetTimestamp(), 10)
	adoptedVal := in.GetValue()
	reg := in.GetRegister()

	newRedisVal := adoptedVal + ":" + time

	val, err := redisClient.Get(reg).Result()
	if err == redis.Nil {
		//idk why this would happen, but just in case
		redisErr := redisClient.Set(reg, newRedisVal, 0).Err()
		if redisErr != nil {
			panic(redisErr)
		}
	} else if err != nil {
		panic(err)
	} else {
		_, oldTimeStamp := parseRedisVal(val)

		//compare the timestamps
		if in.GetTimestamp() >= oldTimeStamp {
			//store the newer value
			redisClient.Set(reg, newRedisVal, 0)
		}
	}

	log.Printf("Sending ACK Read2Request ")
	return &pb.AckResponse{}, nil
}

func (s *server) Write(ctx context.Context, in *pb.WriteRequest) (*pb.AckResponse, error) {
	reg := in.GetRegister()
	val := in.GetValue()
	time := strconv.FormatUint(in.GetTimestampe(), 10)

	newRedisVal := val + ":" + time

	//get value from writer
	log.Printf("WriteRequest Register: " + in.GetRegister() + " Value: " + in.GetValue() + " Time: " + strconv.FormatUint(in.GetTimestampe(), 10))

	//See if we already have a value in that register
	redisVal, redisErr := redisClient.Get(reg).Result()
	if redisErr == redis.Nil {
		//No value, just store the new val
		err := redisClient.Set(reg, newRedisVal, 0).Err()
		if err != nil {
			panic(err)
		}
	} else if redisErr != nil {
		panic(redisErr)
	} else {
		//There is a value here

		//get the stored timestamp
		_, oldTimeStamp := parseRedisVal(redisVal)

		//compare the timestamps
		if in.GetTimestampe() >= oldTimeStamp {
			//store the newer value
			redisClient.Set(reg, newRedisVal, 0)
		} //otherwise leave old value
	}

	//send ACK back to let client know we got it
	log.Printf("Sending ACK WriteRequest")
	return &pb.AckResponse{}, nil
}

func parseRedisVal(val string) (value string, timestamp uint64) {
	s := strings.Split(val, ":")
	timestamp, err := strconv.ParseUint(s[1], 10, 64)

	if err != nil {
		panic(err)
	}
	return s[0], timestamp
}

func main() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := redisClient.Ping().Result()
	log.Printf(pong, err)

	lis, err := net.Listen("tcp", ":"+os.Args[1])
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	log.Printf("gRPC server starting")
	pb.RegisterABDServiceServer(s, &server{})
	log.Printf("ABD Service registered")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
