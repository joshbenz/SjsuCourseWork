package main

import (
	"fmt"
	"time"

	zk "github.com/samuel/go-zookeeper/zk"
)

func main() {
	c, _, err := zk.Connect([]string{"127.0.0.1"}, time.Second) //*10)
	if err != nil {
		panic(err)
	}
	children, stat, err := c.Children("/chain-head")
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v %+v\n", children, stat)

	acl := zk.WorldACL(zk.PermAll)
	path, err := c.Create("/chain-head/JoshuaBenz", []byte("Testing with Golang"), 0, acl)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v \n", path)

	result, stat, err := c.Get("/chain-head/JoshuaBenz")

	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v \n", string(result))

	//e := <-ch
	//fmt.Printf("%+v\n", e)
}
