package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"sort"
	"strconv"
	"strings"

	pb "./api"
	zk "github.com/samuel/go-zookeeper/zk"
	"google.golang.org/grpc"
)

type server struct{}

var dataStore map[string]int32
var hexSessionID string
var uiSessionID uint64
var conn zk.Conn
var xIDCounter uint64
var zkNodeRoot string
var logger *log.Logger
var myAddr string

/*
	I had read that the idomatic way of recreating enums in Go
	was to use global const variables with iota and aliasing the
	type with some type name.
*/

//NodeStatus is the status of my current Node
type NodeStatus int8

//NodeRelation is the relation of a Node to my Node
type NodeRelation int8

//ns_HEAD, ns_LINK, ns_TAIL trying to use as an enum
const (
	nsHEAD NodeStatus = iota
	nsLINK NodeStatus = iota
	nsTAIL NodeStatus = iota
	nsALL  NodeStatus = iota //for the case that I am the only node and thus
	// the head and the tail
)

//used for RC in responses
const (
	SUCCESS uint32 = 0
	FAIL    uint32 = 1
)

//current node status
var nodeStatus NodeStatus

type nodeStub struct {
	addr  string
	hexID string
	uiID  uint64
	stub  pb.TailChainReplicaClient
}

var hexPredecessorID string
var uiPredecessorID uint64
var successorStruct, tailStruct *nodeStub = nil, nil
var sentList []*pb.TailStateUpdateRequest

var clientStub pb.TailClientClient

/*
	NOTE: Apparently in Go, for map get operations, if the key doesn't exist
	the value I ask it to get is automatically 0. Since we are just incrementing
	integers, there is no need to ensure that the key exists. I can just add the
	increment value to 0 and store it in the map. It is interesting that this naturally
	fits our definition of deleted.
*/

func (s *server) ProposeStateUpdate(ctx context.Context, in *pb.TailStateUpdateRequest) (*pb.ChainResponse, error) {
	//if I am the head, I shouldn't be doing this
	if nodeStatus == nsHEAD {
		return &pb.ChainResponse{Rc: FAIL}, nil
	}

	//if i am the only node, then reply to the client
	if nodeStatus == nsALL {
		clientConn, err := grpc.Dial(in.GetHost()+":"+strconv.FormatInt(int64(in.GetPort()), 10), grpc.WithInsecure())
		if err != nil {
			panic(err)
		}
		defer clientConn.Close()
		clientStub = pb.NewTailClientClient(clientConn)

		response, err := clientStub.CxidProcessed(context.Background(), &pb.CxidProcessedRequest{Cxid: in.GetCxid()})
		fmt.Sprint(response.GetRc())

		if err != nil {
			log.Panic("Helooo Client, Are you there?", err)
		}

		if response.GetRc() == SUCCESS {
			//we have success
			sentList = append(sentList, in)
			return &pb.ChainResponse{Rc: SUCCESS}, nil
		}
	}

	//only get state updates from my predecessor
	if uiPredecessorID == in.GetSrc() {
		//update the state
		dataStore[in.GetKey()] = in.GetValue()

		if nodeStatus == nsTAIL || nodeStatus == nsALL {
			//If i am the Tail or the only node
			//respond to the client
			response, err := clientStub.CxidProcessed(context.Background(), &pb.CxidProcessedRequest{Cxid: in.GetCxid()})

			if err != nil {
				log.Panic("Helooo Client, Are you there?", err)
			}

			if response.GetRc() == SUCCESS {
				//we have success
			}
		} else {

			//otherwise propose update to my successor
			log.Printf()
			response, err := successorStruct.stub.ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
				Src:   uiSessionID,
				Xid:   in.GetXid(),
				Key:   in.GetKey(),
				Value: in.GetValue(),
				Host:  in.GetHost(),
				Port:  in.GetPort(),
				Cxid:  in.GetCxid()})

			if err != nil {
				panic(err)
			}

			if response.GetRc() == FAIL {
				log.Fatal("Something went wrong")
			}
		}

		//If we made it this far, then we succeeded, so mark it as sent
		sentList = append(sentList, in)
		return &pb.ChainResponse{Rc: SUCCESS}, nil
	}

	//otherwise, You are no my predecessor, so don't talk to me
	return &pb.ChainResponse{Rc: FAIL}, nil
}

func (s *server) GetLatestXid(ctx context.Context, in *pb.LatestXidRequest) (*pb.LatestXidResponse, error) {
	//only do this if I am the tail
	if nodeStatus == nsTAIL {
		return &pb.LatestXidResponse{Rc: SUCCESS, Xid: sentList[len(sentList)-1].GetXid()}, nil
	}

	//otherwise don't ask me this
	return &pb.LatestXidResponse{Rc: FAIL, Xid: 0}, nil
}

func (s *server) StateTransfer(ctx context.Context, in *pb.TailStateTransferRequest) (*pb.ChainResponse, error) {

	//only get state transfers from my predecessor
	if uiPredecessorID == in.GetSrc() {
		xIDCounter = in.GetStateXid()
		dataStore = in.GetState()
		sentList = in.GetSent()
		return &pb.ChainResponse{Rc: SUCCESS}, nil
	}

	//otherwise, shouldn't be talking to me
	return &pb.ChainResponse{Rc: FAIL}, nil
}

func (s *server) Increment(ctx context.Context, in *pb.TailIncrementRequest) (*pb.HeadResponse, error) {
	//only the head will increment the XID counter
	if nodeStatus == nsHEAD || nodeStatus == nsALL {
		xIDCounter++
	}

	if nodeStatus == nsHEAD || nodeStatus == nsALL {
		key := in.GetKey()
		val, _ := dataStore[key]
		val += in.GetIncrValue()
		dataStore[key] = val

		// propgate it
		response, _ := successorStruct.stub.ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
			Src:   uiSessionID,
			Xid:   xIDCounter,
			Key:   key,
			Value: val,
			Host:  in.GetHost(),
			Port:  in.GetPort(),
			Cxid:  in.GetCxid()})

		if response.GetRc() == SUCCESS {
			//we have success
			return &pb.HeadResponse{Rc: SUCCESS}, nil
		}
	}

	//I'm not the head
	return &pb.HeadResponse{Rc: FAIL}, nil
}

func (s *server) Delete(ctx context.Context, in *pb.TailDeleteRequest) (*pb.HeadResponse, error) {
	//only the head will increment the XID counter
	if nodeStatus == nsHEAD || nodeStatus == nsALL {
		xIDCounter++
	}

	//if I am the only node, then just reply to the client
	/*if nodeStatus == nsALL {
		clientConn, err := grpc.Dial(in.GetHost()+":"+strconv.FormatInt(int64(in.GetPort()), 10), grpc.WithInsecure())
		if err != nil {
			panic(err)
		}
		defer clientConn.Close()
		clientStub = pb.NewTailClientClient(clientConn)
		response, err := clientStub.CxidProcessed(context.Background(), &pb.CxidProcessedRequest{Cxid: in.GetCxid()})

		if err != nil {
			log.Panic("Helooo Client, Are you there?", err)
		}

		if response.GetRc() == SUCCESS {
			//we have success
		}
	}*/

	if nodeStatus == nsHEAD || nodeStatus == nsALL {
		key := in.GetKey()
		//set to 0
		dataStore[key] = 0

		if nodeStatus == nsALL {
			//I am the head and tail, no need to propogate results
			return &pb.HeadResponse{Rc: SUCCESS}, nil
		}
		//Otherwise, begin the propogation of results down the chain

		response, _ := successorStruct.stub.ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
			Src:   uiSessionID,
			Xid:   xIDCounter,
			Key:   key,
			Value: 0,
			Host:  in.GetHost(),
			Port:  in.GetPort(),
			Cxid:  in.GetCxid()})

		if response.GetRc() == SUCCESS {
			//we have success
		}
		return &pb.HeadResponse{Rc: SUCCESS}, nil
	}

	//I'm not the head
	return &pb.HeadResponse{Rc: FAIL}, nil
}

func (s *server) Get(ctx context.Context, in *pb.GetRequest) (*pb.GetResponse, error) {
	if nodeStatus == nsTAIL || nodeStatus == nsALL { //I am the tail or the head and the tail
		key := in.GetKey()
		val, _ := dataStore[key]
		return &pb.GetResponse{Rc: SUCCESS, Value: int32(val)}, nil
	}

	//otherwise don't talk to me
	return &pb.GetResponse{Rc: FAIL, Value: 0}, nil
}

func (s *server) ZkChildrenChange(ctx context.Context, in *pb.ZkChildrenChangeRequest) (*pb.ZkChildrenChangeResponse, error) {
	uiSessionID = in.GetUiSessionID()
	hexSessionID = in.GetHexSessionID()
	establishHierarchy(in.GetChildren(), in.GetZkNodeRoot())
	return &pb.ZkChildrenChangeResponse{}, nil
}

func establishHierarchy(children []string, zkPath string) {
	//sort children
	sort.Slice(children, func(i, j int) bool {
		return hexToUI(children[i]) < hexToUI(children[j])
	})
	logger.Printf("Sorted Children: %+v", children)

	mySessionIDIndex := find(children, hexSessionID)
	//set the role of my node
	if mySessionIDIndex == 0 {
		nodeStatus = nsHEAD
		if len(children) == 1 { //I am also the tail
			nodeStatus = nsALL
		}
	} else if mySessionIDIndex+1 >= len(children) {
		nodeStatus = nsTAIL
	} else {
		nodeStatus = nsLINK
	}
	log.Printf("My Node Status: %+v", statusToString())

	//update predecessor
	//when i'm not the head and also not the only node in the chain
	if nodeStatus != nsHEAD && nodeStatus != nsALL {
		//if my predecessor changed
		if hexPredecessorID != children[mySessionIDIndex-1] {
			//track the new predecessor
			hexPredecessorID = children[mySessionIDIndex-1]
			uiPredecessorID = hexToUI(children[mySessionIDIndex-1])
		}
	} else {
		//do nothing, the head has no predecessor
	}

	//If I am the only node, tail and successor will both be me
	var node nodeStub
	var hexSuccessorID string

	if nodeStatus != nsALL && nodeStatus != nsTAIL {
		hexSuccessorID = children[mySessionIDIndex+1]
		uiSuccessorID := hexToUI(hexSuccessorID)

		//Update successor if needed,
		if successorStruct == nil {
			//haven't had a successor yet, so make one
			addr := extractZkNodeData(hexSuccessorID, zkPath)
			node = nodeStub{addr, hexSuccessorID, uiSuccessorID, getClientStub(addr)}
			successorStruct = &node
		} else {
			//there was a successor
			//is it the same successor as before
			if successorStruct.uiID != uiSuccessorID {
				//not the same successor
				//update the successor
				addr := extractZkNodeData(hexSuccessorID, zkPath)
				node = nodeStub{addr, hexSuccessorID, uiSuccessorID, getClientStub(addr)}
				successorStruct = &node
			} else {
				//it is the same successor, so do nothing
			}
		}

		var tailNode nodeStub
		//update Tail
		hexTailID := children[len(children)-1]
		uiTailID := hexToUI(hexTailID)
		if tailStruct == nil {
			//we haven't had a tail yet, so make one
			addr := extractZkNodeData(hexTailID, zkPath)
			tailNode = nodeStub{addr, hexTailID, uiTailID, getClientStub(addr)}
			tailStruct = &tailNode
		} else {
			//there was a tail
			//is it the same tail as before
			if tailStruct.uiID != uiTailID {
				//not the same tail, so update it
				addr := extractZkNodeData(hexTailID, zkPath)
				tailNode = nodeStub{addr, hexTailID, uiTailID, getClientStub(addr)}
				tailStruct = &tailNode
			} else {
				//it is the same tail, so do nothing
			}
		}
	} else if nodeStatus == nsTAIL {
		//i am the tail
		hexSuccessorID = children[len(children)-1]
		uiSuccessorID := hexToUI(hexSuccessorID)
		//Update successor if needed,
		if successorStruct == nil {
			//haven't had a successor yet, so make one
			addr := extractZkNodeData(hexSuccessorID, zkPath)
			node = nodeStub{addr, hexSuccessorID, uiSuccessorID, getClientStub(addr)}
			successorStruct = &node
		} else {
			//there was a successor
			//is it the same successor as before
			if successorStruct.uiID != uiSuccessorID {
				//not the same successor
				//update the successor
				addr := extractZkNodeData(hexSuccessorID, zkPath)
				node = nodeStub{addr, hexSuccessorID, uiSuccessorID, getClientStub(addr)}
				successorStruct = &node
			} else {
				//it is the same successor, so do nothing
			}
		}

	} else {
		//i am the only node so I am the successor
		n := nodeStub{myAddr, hexSessionID, uiSessionID, getClientStub(myAddr)}
		successorStruct = &n
	}
}

func extractZkNodeData(id string, zkpath string) string {
	zkNodeByteData, _, err := conn.Get(zkpath + id)
	if err != nil {
		panic(err)
	}
	zkNodeStringData := string(zkNodeByteData)
	return zkNodeStringData
}

func getClientStub(addr string) pb.TailChainReplicaClient {
	clientConn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer clientConn.Close()
	client := pb.NewTailChainReplicaClient(clientConn)
	return client
}

func find(a []string, x string) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return len(a)
}

func hexToUI(hexNum string) uint64 {
	ui, err := strconv.ParseUint(hexNum, 16, 64)
	if err != nil {
		panic(err)
	}
	return ui
}

func statusToString() string {
	var result string
	if nodeStatus == nsHEAD {
		result = "Head Node (" + hexSessionID + ")"
	} else if nodeStatus == nsTAIL {
		result = "Tail Node (" + hexSessionID + ")"
	} else if nodeStatus == nsLINK {
		result = "Link Node (" + hexSessionID + ")"
	} else if nodeStatus == nsALL {
		result = "I am the only Node (" + hexSessionID + ")"
	} else {
		result = ""
	}
	return result
}

/*
	ARGS:
	My addr in form of addr:port
*/
func main() {
	logger = log.New(os.Stdout, "", 0)
	//initialize hashmap
	dataStore = make(map[string]int32)
	myAddr = os.Args[1]

	//gRPC init

	port := strings.Split(myAddr, ":")
	lis, err := net.Listen("tcp", ":"+port[1])
	if err != nil {
		log.Fatalf("Failed to listen on: %v", err)
	}

	s := grpc.NewServer()
	log.Printf("gRPC server starting")

	pb.RegisterTailChainReplicaServer(s, &server{})
	pb.RegisterZKClientServer(s, &server{})
	log.Printf("Chain Replica (Tail Version) Service registered")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
