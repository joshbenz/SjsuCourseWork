package main

import (
	"context"
	"log"
	"os"
	"strconv"
	"time"

	pb "../api"
	zk "github.com/samuel/go-zookeeper/zk"
	"google.golang.org/grpc"
)

func main() {
	zkAddr := os.Args[1]
	myAddr := os.Args[2]
	zkNodeRoot := os.Args[3]

	//zoo keeper init

	conn, _, err := zk.Connect([]string{zkAddr}, time.Second*10)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	//Originally, it was executing so fast that I wouldn't get assigned a session ID.
	//So I have to sleep and let zookeeper assign me one
	time.Sleep(5 * time.Second)
	log.Printf("int64 Session ID: %+v", conn.SessionID())

	uiSessionID := uint64(conn.SessionID())
	hexSessionID := strconv.FormatUint(uiSessionID, 16)

	log.Printf("Connected to %+v at %+v with Hex Session Id: %+v \n", zkNodeRoot, zkAddr, hexSessionID)

	acl := zk.WorldACL(zk.PermAll)

	//should probably check if it already exists
	pathExists, _, err := conn.Exists(zkNodeRoot)
	if err != nil {
		panic(err)
	}

	if !pathExists {
		path, _ := conn.Create(zkNodeRoot, []byte{}, zk.FlagEphemeral, acl)
		log.Printf("Created path %+v", path)
	}

	//register link in chain
	path, err := conn.Create(zkNodeRoot+"/"+hexSessionID, []byte(myAddr), zk.FlagEphemeral, acl)

	if err != nil {
		panic(err)
	}
	log.Printf("Created path %+v", path)

	clientConn, err := grpc.Dial(myAddr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer clientConn.Close()
	client := pb.NewZKClientClient(clientConn)

	for {
		children, _, watcher, err := conn.ChildrenW(zkNodeRoot)
		if err != nil {
			log.Printf("watch children path error, path:%s, err:%v", zkNodeRoot, err)
			continue
		}
		log.Printf("Changes Detected")

		client.ZkChildrenChange(context.Background(), &pb.ZkChildrenChangeRequest{
			Children:     children,
			UiSessionID:  uiSessionID,
			HexSessionID: hexSessionID,
			ZkNodeRoot:   zkNodeRoot})

		select {
		case <-watcher:
		}
	}
}
