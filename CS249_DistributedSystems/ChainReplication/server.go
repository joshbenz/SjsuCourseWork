package main

import (
	"context"
	"log"
	"net"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	pb "./api"
	zk "github.com/samuel/go-zookeeper/zk"
	"google.golang.org/grpc"
)

type server struct{}

var dataStore map[string]int32
var nodeRouting map[string]pb.TailChainReplicaClient
var clientRouting map[string]pb.TailClientClient
var hexSessionID string
var uiSessionID uint64
var conn *zk.Conn
var xIDCounter uint64
var zkNodeRoot string
var logger *log.Logger
var lock = sync.RWMutex{}

/*
	I had read that the idomatic way of recreating enums in Go
	was to use global const variables with iota and aliasing the
	type with some type name.
*/

//NodeStatus is the status of my current Node
type NodeStatus int8

//ns_HEAD, ns_LINK, ns_TAIL trying to use as an enum
const (
	nsHEAD NodeStatus = iota
	nsLINK NodeStatus = iota
	nsTAIL NodeStatus = iota
	nsALL  NodeStatus = iota //for the case that I am the only node and thus
	// the head and the tail
)

//used for RC in responses
const (
	SUCCESS uint32 = 0
	FAIL    uint32 = 1
)

//current node status
var nodeStatus NodeStatus

var hexPredecessorID, hexSuccessorID, hexTailID string
var uiPredecessorID uint64
var sentList []*pb.TailStateUpdateRequest

var clientStub pb.TailClientClient

/*
	NOTE: Apparently in Go, for map get operations, if the key doesn't exist
	the value I ask it to get is automatically 0. Since we are just incrementing
	integers, there is no need to ensure that the key exists. I can just add the
	increment value to 0 and store it in the map. It is interesting that this naturally
	fits our definition of deleted.
*/

func (s *server) ProposeStateUpdate(ctx context.Context, in *pb.TailStateUpdateRequest) (*pb.ChainResponse, error) {
	log.Printf("Propose Update")
	if nodeStatus == nsHEAD {
		return &pb.ChainResponse{Rc: FAIL}, nil //I dont process this
	}

	addr := in.GetHost() + ":" + strconv.FormatInt(int64(in.GetPort()), 10)
	lock.RLock()
	stub, ok := clientRouting[addr]
	lock.RUnlock()

	if !ok {
		stub = getClientStub(addr)
		lock.Lock()
		clientRouting[addr] = stub
		lock.Unlock()
	}

	if nodeStatus == nsALL {
		//nothing to do but to reply to client
		dataStore[in.GetKey()] = in.GetValue() //update for consistency
		//reply
		response, err := stub.CxidProcessed(context.Background(), &pb.CxidProcessedRequest{Cxid: in.GetCxid()})
		if err != nil {
			panic(err)
		}

		if response.GetRc() == SUCCESS {
			//we have success
		}
		return &pb.ChainResponse{Rc: SUCCESS}, nil
	}

	if uiPredecessorID == in.GetSrc() {
		if nodeStatus == nsTAIL {

			//update and reply to client
			lock.Lock()
			dataStore[in.GetKey()] = in.GetValue()
			lock.Unlock()

			//reply
			response, err := stub.CxidProcessed(context.Background(), &pb.CxidProcessedRequest{Cxid: in.GetCxid()})
			if err != nil {
				panic(err)
			}

			if response.GetRc() == SUCCESS {
				//we have success
			}
			sentList = append(sentList, in)
			return &pb.ChainResponse{Rc: SUCCESS}, nil
		}

		if nodeStatus == nsLINK {
			lock.Lock()
			dataStore[in.GetKey()] = in.GetValue()
			lock.Unlock()

			response, err := nodeRouting[hexSuccessorID].ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
				Src:   uiSessionID,
				Xid:   in.GetXid(),
				Key:   in.GetKey(),
				Value: in.GetValue(),
				Host:  in.GetHost(),
				Port:  in.GetPort(),
				Cxid:  in.GetCxid()})

			if err != nil {
				panic(err)
			}

			if response.GetRc() == SUCCESS {
				//success
			} else {
				//Failed
			}

			sentList = append(sentList, in)
			return &pb.ChainResponse{Rc: SUCCESS}, nil
		}
	} else {
		//you are not my predecessor
	}
	return &pb.ChainResponse{Rc: FAIL}, nil
}

func (s *server) GetLatestXid(ctx context.Context, in *pb.LatestXidRequest) (*pb.LatestXidResponse, error) {
	log.Printf("GetLatestXid")
	//only do this if I am the tail
	var xid uint64

	if len(sentList) == 0 {
		xid = 0
	} else {
		xid = sentList[len(sentList)-1].GetXid()
	}

	if nodeStatus == nsTAIL {
		return &pb.LatestXidResponse{Rc: SUCCESS, Xid: xid}, nil
	}

	//otherwise don't ask me this
	return &pb.LatestXidResponse{Rc: FAIL, Xid: 0}, nil
}

func (s *server) StateTransfer(ctx context.Context, in *pb.TailStateTransferRequest) (*pb.ChainResponse, error) {
	log.Printf("StateTransfer")
	//only get state transfers from my predecessor
	if uiPredecessorID == in.GetSrc() {
		xIDCounter = in.GetStateXid()
		dataStore = in.GetState()
		sentList = in.GetSent()
		return &pb.ChainResponse{Rc: SUCCESS}, nil
	}

	//otherwise, shouldn't be talking to me
	return &pb.ChainResponse{Rc: FAIL}, nil
}

func (s *server) Increment(ctx context.Context, in *pb.TailIncrementRequest) (*pb.HeadResponse, error) {
	log.Printf("Increment")
	if nodeStatus == nsHEAD {
		atomic.AddUint64(&xIDCounter, 1)
		key := in.GetKey()
		lock.RLocker()
		val, _ := dataStore[key]
		lock.RUnlock()
		val += in.GetIncrValue()
		lock.Lock()
		dataStore[key] = val
		lock.Lock()

		// propgate it
		response, err := nodeRouting[hexSuccessorID].ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
			Src:   uiSessionID,
			Xid:   xIDCounter,
			Key:   key,
			Value: val,
			Host:  in.GetHost(),
			Port:  in.GetPort(),
			Cxid:  in.GetCxid()})

		if err != nil {
			panic(err)
		}

		if response.GetRc() == SUCCESS {
			//we have success, the job has started
		}
		return &pb.HeadResponse{Rc: SUCCESS}, nil
	}

	if nodeStatus == nsALL {
		atomic.AddUint64(&xIDCounter, 1)
		key := in.GetKey()
		lock.RLock()
		val, _ := dataStore[key]
		lock.RUnlock()
		val += in.GetIncrValue()
		lock.Lock()
		dataStore[key] = val
		lock.Unlock()

		res, err := s.ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
			Src:   uiSessionID,
			Xid:   xIDCounter,
			Key:   key,
			Value: val,
			Host:  in.GetHost(),
			Port:  in.GetPort(),
			Cxid:  in.GetCxid()})

		if err != nil {
			panic(err)
		}

		if res.GetRc() == SUCCESS {
			//we have success, the job has started
		}
		return &pb.HeadResponse{Rc: SUCCESS}, nil
	}

	//I'm not the head
	return &pb.HeadResponse{Rc: FAIL}, nil
}

func (s *server) Delete(ctx context.Context, in *pb.TailDeleteRequest) (*pb.HeadResponse, error) {
	log.Printf("Delete")
	if nodeStatus == nsHEAD {
		atomic.AddUint64(&xIDCounter, 1)
		key := in.GetKey()
		lock.Lock()
		dataStore[key] = 0
		lock.Unlock()

		// propgate it to successor
		response, err := nodeRouting[hexSuccessorID].ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
			Src:   uiSessionID,
			Xid:   xIDCounter,
			Key:   key,
			Value: dataStore[key],
			Host:  in.GetHost(),
			Port:  in.GetPort(),
			Cxid:  in.GetCxid()})

		if err != nil {
			panic(err)
		}

		if response.GetRc() == SUCCESS {
			//we have success, the job has started
		}
		return &pb.HeadResponse{Rc: SUCCESS}, nil
	}

	if nodeStatus == nsALL {
		atomic.AddUint64(&xIDCounter, 1)
		key := in.GetKey()
		lock.Lock()
		dataStore[key] = 0
		lock.Unlock()

		res, _ := s.ProposeStateUpdate(context.Background(), &pb.TailStateUpdateRequest{
			Src:   uiSessionID,
			Xid:   xIDCounter,
			Key:   key,
			Value: dataStore[key],
			Host:  in.GetHost(),
			Port:  in.GetPort(),
			Cxid:  in.GetCxid()})

		if res.GetRc() == SUCCESS {
			//we have success, the job has started
		}
		return &pb.HeadResponse{Rc: SUCCESS}, nil
	}

	//I'm not the head
	return &pb.HeadResponse{Rc: FAIL}, nil
}

func (s *server) Get(ctx context.Context, in *pb.GetRequest) (*pb.GetResponse, error) {
	log.Printf("Get")
	if nodeStatus == nsTAIL || nodeStatus == nsALL { //I am the tail or the head and the tail
		key := in.GetKey()
		lock.RLock()
		val, _ := dataStore[key]
		lock.RUnlock()
		return &pb.GetResponse{Rc: SUCCESS, Value: int32(val)}, nil
	}

	//otherwise don't talk to me
	return &pb.GetResponse{Rc: FAIL, Value: 0}, nil
}

func (s *server) ZkChildrenChange(ctx context.Context, in *pb.ZkChildrenChangeRequest) (*pb.ZkChildrenChangeResponse, error) {
	uiSessionID = in.GetUiSessionID()
	hexSessionID = in.GetHexSessionID()
	establishHierarchy(in.GetChildren(), in.GetZkNodeRoot()+"/")
	return &pb.ZkChildrenChangeResponse{}, nil
}

func establishHierarchy(children []string, zkPath string) {
	var stateTransfer bool = false
	//sort children
	sort.Slice(children, func(i, j int) bool {
		return hexToUI(children[i]) < hexToUI(children[j])
	})
	logger.Printf("Sorted Children: %+v", children)

	mySessionIDIndex := find(children, hexSessionID)

	//this method fires with every change in the zookeeper nodes list
	//in the case that nodes are only removed, it is not necessary to
	//do a state transfer. Only do a state transfer if I was orignally
	//the tail and now I am one away from the tail or I was orignally
	//the only node in the chain
	if nodeStatus == nsTAIL || nodeStatus == nsALL {
		if mySessionIDIndex == len(children)-2 { //someone was added on to the chain
			stateTransfer = true
		}
	}

	//set the role of my node
	if mySessionIDIndex == 0 {
		nodeStatus = nsHEAD
		if len(children) == 1 { //I am also the tail
			nodeStatus = nsALL
		}
	} else if mySessionIDIndex+1 >= len(children) {
		nodeStatus = nsTAIL
	} else {
		nodeStatus = nsLINK
	}
	logger.Printf("My Node Status: %+v", statusToString())

	//HEAD has no predecessor
	//HEAD has a successor
	//If it is the only node, it is the tail
	if nodeStatus == nsHEAD { //the Head, but chain has links
		//HEAD has no predecessor. I am my predecessor
		hexPredecessorID = ""
		//Head has a Successor, next node in list
		hexSuccessorID = children[mySessionIDIndex+1]
		hexTailID = children[len(children)-1]
	}

	//A link in the chain
	if nodeStatus == nsLINK {
		//LINK has a predecessor and a successor and a TAIL
		//predecessor
		hexPredecessorID = children[mySessionIDIndex-1] //the previous id
		uiPredecessorID = hexToUI(hexPredecessorID)

		//successor is the next id
		hexSuccessorID = children[mySessionIDIndex+1]

		//tail
		hexTailID = children[len(children)-1] //the last id
	}

	if nodeStatus == nsTAIL {
		//Tail has a predecessor
		hexPredecessorID = children[mySessionIDIndex-1]
		uiPredecessorID = hexToUI(hexPredecessorID)

		//Tail has no successor
		hexSuccessorID = ""
		hexTailID = ""
	}

	if nodeStatus == nsALL {
		//i am everything
		hexPredecessorID = ""
		hexSuccessorID = ""
		hexTailID = ""
	}

	if len(hexSuccessorID) > 0 {
		_, ok := nodeRouting[hexSuccessorID]
		if !ok {
			addr := extractZkNodeData(hexSuccessorID, zkPath)
			newStub := getChainStub(addr)
			lock.Lock()
			nodeRouting[hexSuccessorID] = newStub
			lock.Unlock()
		}
	}

	if len(hexTailID) > 0 {
		_, ok := nodeRouting[hexTailID]
		if !ok {
			addr := extractZkNodeData(hexTailID, zkPath)
			newStub := getChainStub(addr)
			lock.Lock()
			nodeRouting[hexTailID] = newStub
			lock.Unlock()
		}
	}

	if stateTransfer {
		stub, ok := nodeRouting[hexPredecessorID]
		if ok {
			stub.StateTransfer(context.Background(), &pb.TailStateTransferRequest{
				Src:      uiSessionID,
				StateXid: xIDCounter,
				State:    dataStore,
				Sent:     sentList})
		} else {
			addr := extractZkNodeData(hexTailID, zkPath)
			stub := getChainStub(addr)
			lock.Lock()
			nodeRouting[hexTailID] = stub
			lock.Unlock()
			stub.StateTransfer(context.Background(), &pb.TailStateTransferRequest{
				Src:      uiSessionID,
				StateXid: xIDCounter,
				State:    dataStore,
				Sent:     sentList})

		}

	}

	//I don't really have a good place to check for Xids that can be removed
	//So i'll do it here since this gets called periodically

	if len(sentList) > 5 { //why not
		response, err := nodeRouting[hexTailID].GetLatestXid(context.Background(), &pb.LatestXidRequest{})
		if err != nil {
			panic(err)
		}
		if response.GetRc() == SUCCESS {
			latestXid := response.GetXid()
			//purge the sent list up to this xid
			for i := range sentList {
				if sentList[i].GetXid() != latestXid {
					//remove it
					copy(sentList[i:], sentList[i+1:])
					sentList[len(sentList)-1] = nil
					sentList = sentList[:len(sentList)-1]
				} else {
					//we made it to the current latest xid so stop
					break
				}
			}
		}
	}
}

func extractZkNodeData(id string, zkpath string) string {
	zkNodeByteData, _, err := conn.Get(zkpath + id)
	if err != nil {
		panic(err)
	}
	zkNodeStringData := string(zkNodeByteData)
	return zkNodeStringData
}

func getChainStub(addr string) pb.TailChainReplicaClient {
	clientConn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		panic(err)
	}
	client := pb.NewTailChainReplicaClient(clientConn)
	return client
}

func getClientStub(addr string) pb.TailClientClient {
	clientConn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := pb.NewTailClientClient(clientConn)
	return client
}

func find(a []string, x string) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return len(a)
}

func hexToUI(hexNum string) uint64 {
	if strings.Contains(hexNum, "x") {
		hexNum = string(hexNum[0])
	}

	ui, err := strconv.ParseUint(hexNum, 16, 64)
	if err != nil {
		panic(err)
	}
	return ui
}

func statusToString() string {
	var result string
	if nodeStatus == nsHEAD {
		result = "Head Node (" + hexSessionID + ")"
	} else if nodeStatus == nsTAIL {
		result = "Tail Node (" + hexSessionID + ")"
	} else if nodeStatus == nsLINK {
		result = "Link Node (" + hexSessionID + ")"
	} else if nodeStatus == nsALL {
		result = "I am the only Node (" + hexSessionID + ")"
	} else {
		result = ""
	}
	return result
}

func init() {
	dataStore = make(map[string]int32)
	nodeRouting = make(map[string]pb.TailChainReplicaClient)
	clientRouting = make(map[string]pb.TailClientClient)
}

/*
	ARGS:
	My addr in form of addr:port
*/
func main() {
	logger = log.New(os.Stdout, "", 0)
	//initialize hashmap

	myAddr := os.Args[1]
	zkAddr := os.Args[2]

	//Zookeeper for getting individual nodes
	zconn, _, err := zk.Connect([]string{zkAddr}, time.Second*10)
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	conn = zconn

	//gRPC init

	port := strings.Split(myAddr, ":")
	lis, err := net.Listen("tcp", ":"+port[1])
	if err != nil {
		logger.Fatalf("Failed to listen on: %v", err)
	}

	s := grpc.NewServer()
	logger.Printf("gRPC server starting")

	pb.RegisterTailChainReplicaServer(s, &server{})
	pb.RegisterZKClientServer(s, &server{})
	logger.Printf("Chain Replica (Tail Version) Service registered")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
