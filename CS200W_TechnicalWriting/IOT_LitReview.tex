\documentclass[12]{article}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{amsfonts,amsmath,amssymb}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{float}
\usepackage{transparent}
\usepackage{eso-pic}
\usepackage[tiny]{titlesec}
\usepackage{blindtext}
\usepackage{sectsty}
\usepackage{setspace}
\usepackage{titlesec}
\doublespacing
\usepackage{indentfirst}
\sectionfont{\centering \normalsize \normalfont}
\usepackage{tocloft}
\renewcommand{\cftsecfont}{\normalsize \normalfont}
\renewcommand{\cftsecleader}{\normalsize \normalfont \cftdotfill{\cftdotsep}}
\renewcommand{\contentsname}{\hfill \normalsize \normalfont TABLE OF CONTENTS}
\renewcommand{\cftaftertoctitle}{\hfill}


\fancypagestyle{mypagestyle}{%
\fancyhf{} 
\renewcommand{\headrulewidth}{0pt}
\lhead{ \tiny \MakeUppercase{{\tiny  A Literature Review of Artificially Intelligent Medical Diagnosis with Explainable AI}}}
\rhead{}
\setlength{\footskip}{10pt}

\cfoot{\thepage}
}



\pagestyle{fancy}
\fancyhf{} 
\renewcommand{\headrulewidth}{0pt}
\lhead{ \MakeUppercase{{\tiny A Literature Review of Artificially Intelligent Medical Diagnosis with Explainable AI}}}
\rhead{\scriptsize{\thepage}}

\renewcommand{\thesection}{\Roman{section}. } 
\renewcommand{\thesubsection}{\Alph{subsection}.}


\sectionfont{\centering\normalsize\normalfont\MakeUppercase}

\setlength{\headheight}{.5in}
\setlength{\headsep}{.5in}



\begin{document}
\begin{titlepage}

\pagenumbering{roman}

\setlength{\parindent}{0pt}
    \vspace*{-3.8\baselineskip}
    \MakeUppercase{{\tiny A Literature Review of Artificially Intelligent Medical Diagnosis with Explainable AI}} \hfill 
    \begin{center}
    
    \vfill
    A Literature Review of Artificially Intelligent Medical Diagnosis with Explainable AI\\
    \vfill
    
    A Literature Review\\
    \vfill
    
    Presented to  \\
    Professor Sharon Stranahan\\
    \vfill
    
    Department of Computer Science\\
    San Jos\'e State University\\
    \vfill
    
    In Partial Fulfillment\\
    Of the Requirements for the Class\\
    CS 200W\\
    \vfill
    
    By\\ 
    Joshua Benz\\
    December, 2019\\
\end{center}
\end{titlepage}

\begin{abstract}
\thispagestyle{mypagestyle}
As artificial intelligence moves into domains where decisions have very real impacts, it has become increasingly more important to understand its reasoning behind a conclusion. There are typically two classes of artificial intelligence, classical and modern, that have strengths and weaknesses in a model's adaptability and transparency. This literature review aims to study the dominant approaches used to achieve explainability, along with their strengths and weaknesses, particularly in the context of the medical domain. This review looks at various techniques for tracing the decision making process for symbolic and numerical-based systems. It also explores recent progress made through hybrid techniques derived from past approaches. Although these techniques have proven to be useful to understanding the decision process in certain contexts, this review concludes that there is a need to develop better techniques for transparency with modern artificial intelligence, especially in the medical domain. Future work could include hybrid approaches for generating explanations.
\end{abstract}
\newpage
\tableofcontents
\thispagestyle{mypagestyle}
\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}
\setlength{\parindent}{4em}
\renewcommand{\baselinestretch}{1.5}

\section{Introduction}
Artificial Intelligence (AI) has secured its role as critical to modern society due to its predictive power, which often outperforms that of humans. Throughout this review, classical AI will be used to refer to mainly inference engines that apply logical rules to semantic content to try to mimic human reasoning, which was the dominant approach found in the 1970's to 1990's. Modern AI will be used to refer to methods that utilize numerical classification of data such as deep learning methods. Modern, numerical-based, AI is considered flexible since it can adapt to new data automatically and discover new knowledge from data. However, has a problem that is preventing it from entering other critical areas such as the medical field. When evaluating modern AI, it can be difficult to ascertain how it arrived at its particular result. It performs well and has advantages over classical AI, but it can be hard to understand its reasoning. In contrast, classical AI from the 1970's, such as expert systems, is knowledge-based [1] which makes it inflexible since it is up to the domain experts to adapt the knowledge base themselves.This means that domain experts created a structured source of knowledge pertaining to a particular domain that the algorithm can to follow preconceived rules to infer conclusions. This also means that there is no knowledge discovery, and thus the conclusions can only be inferred from the knowledge that is provided. Since there is an obvious base of knowledge, it would be trivial for the algorithm to show what inferences were made to arrive at the current result. In other words, the algorithm is transparent. Transparency is particularly useful for domains that require predictions and diagnostics as to how the prediction was made. Modern AI, however, lacks this transparency and thus it can be difficult to accept the results as correct without knowing how it inferred them.

The ability for the AI algorithm to be transparent by showing its reasoning in such a way that humans can understand is called explainability.  Explanation allows for users to follow the results confidently because they can verify said results by following the AI's logic. This establishes a much needed trust between AI and users [2]. In the medical field, it would be uncomfortable to accept a diagnosis from an algorithm where the inferences made cannot be seen. Explainable Artificial Intelligence (XAI) was researched heavily in the 1970's and 1980's mainly in the form of expert systems, but was dropped for the highly adaptable modern approaches. In the late 2010's XAI is making a resurgence to help establish trust between users and the result given from an algorithm in critical areas, one of which is medical diagnosis.

In this literature review, we focus on exploring XAI and XAI in relation to the medical field. This includes research that has been done and new techniques that are still being explored. The key research questions are as follows: What is XAI? Why do we need XAI? What is XAI's role in the medical field? What strategies and techniques are being researched to advance XAI and XAI in the medical field? The survey uses scholarly articles from journals, published papers and references from books.

The rest of the literature review has the following organization: Section II focuses on modern and classical AI and their benefits from developer and user perspectives. Section III focuses on XAI in the medical domain and what research is happening there. Finally, Section IV talks about what is being done and what needs to be done to further advance XAI.

\begin{figure}
\centering
        \includegraphics[scale=.75]{XAI.png}
    \caption{Organization of the literature survey.}
\end{figure}

\section{Transparency and Artificial Intelligence}
\par
Modern and classical AI showcase the trade-offs made when choosing between flexible predictive power with little transparency and lack of flexibility with transparency of reasoning. Realizing the reasons behind researching XAI is better seen through these trade-offs. 
\subsection{Classical AI: User and Developer Perspectives}
The main result of classical AI is the creation of expert systems, which had a focus on achieving transparency by being able to trace the reasoning behind a result. Expert systems is a method of symbolic reasoning that utilizes user-created knowledge from an expert in a particular domain, and organizes that knowledge such that the expert reasoning can be performed algorithmically via inference engines. Since there is a clear base of knowledge that the computer program uses, explanations can be provided by following the inferences that the algorithm made to reach a result. This reasoning should model that of experts in that particular domain. 

The main limiting factor of classical AI is its lack of ability to adapt to situations and present explanations that can be limiting due to the user's level of knowledge. Kass and Finin formally define what makes a good explanation and propose a model in which the system tailors explanations towards the user's knowledge level based on how that user interacted with it [3]. This emphasizes that the value of an expert system lies in its transparency, which depends heavily on the knowledge of the user interpreting it. However, this still does not address the inflexibility of a predefined knowledge base due to its inability to adapt itself to make new correlations without significant human intervention. Kass and Finin were able to address the user perspective, but not the developer's perspective.

Buchanan and Shortliffe introduce Mycin which uses meta-knowledge to enhance the explanability for both the user and the developers, in the context of the medical domain [4]. They agree with Kass and Finin [3] in that the user perspective should be tailored to the individual's level of knowledge for the best level of explainability. With this meta-knowledge you could try to provide context to the algorithm and produce meta-rules so that they could control the level of explainability when backtracing the inferences made to reach a decision. With this approach, the explainability for a user would not the be the same as for a developer or an expert. Developers could get useful debugging information without needing to go through the user perspective.

Swartout expands on the idea of meta-knowledge and introduces the separation of domain knowledge from algorithmic specific knowledge. He addresses problems such as lack of justifications due to gaps in the knowledge base and the insufficient explanations provided by expert systems similar to what Buchanan and Shortliffe were addressing [5]. The inflexibility of classical AI leads to the gaps that Swartout discusses. Gaps in the knowledge base lead to gaps in the justifications used to come to a conclusion. The use of meta-knowledge allowed for the generation of easily understandable responses for both users and developers while maintaining the relevancy of the results similar to Kass and Finin [3].

\subsection{Modern AI: User and Developer Perspectives}
A common theme found in classical AI is explainability is being able to generate explanations based on context of who the user is and what their level of knowledge in the domain is. Information for developers trying to debug the systems and users trying to use the systems are usually not the same. Modern AI has become a powerful tool and it is clear that the conclusions given from an AI must be translated into domain specific knowledge. There is also a shift in terminology. When talking about modern AI, the interest lies in interpretability [6]. Bratko suggests that modern AI acts as a tool for discovery. The literature appears to agree on this terminology because there is a slight shift in focus in the goals of modern AI. The field is interested not only in accurate predictions from the AI, but also the implications of the AI reaching that particular conclusion. In other words, predicting a numerical value isn't as useful as what the numerical value means in a given context. These interpretation create the explanations. The difficulty with transparency comes from the nature of modern AI being numerically-based instead of symbolically.

Modern AI excels in predictive power but suffers in algorithmic transparency. The ability to see what patterns of neurons fire in an Artificial Neural Network (ANN) usually isn't useful for developers or users. Lou, Caruana, and Gehrke realized that in order to achieve explainability, humans also need to be able to understand how the model works, which is similar to algorithmic transparency [7]. This is difficult since these models weren't explicitly designed to be transparent. However, they have become integral to parts of society and business such as finance, transportation and the medical field.

Ribeiro, being discouraged by the idea of true transparency being unobtainable, was inspired by the classical AI approach of being able to trace inference paths to see reasoning. With a similar goal, he proposed his Local Interpretable Model-Agnostic Explanations (LIME) framework, which worked on image and text classification, to locally identify clusters of heavily weighted pixels to see what areas are being most taken into consideration when making predictions [8]. This has become a dominant approach since 2017. Montavon expanded on this to create heatmaps for trying to interpret multilayer neural networks used for image classification by backpropogating explanations through the network. He could create a 'hot to cold' scale for identifying what weights played the more crucial roles in the outputs [9]. Kumar continues the trend by proposing 
CLass-Enhanced Attentive Response (CLEAR) which is used to visualize dominant classes associated with heavily weighted areas of the images with Deep Neural Networks (DNN) [10]. This is an improvement to heatmaps because it yields multiple options of what usually happens in the DNN decision process as opposed to just one option. Although these options give some algorithmic transparency, it is still up to a particular user to interpret what the results mean.

Researchers agree that transparency with these AI systems is likely unobtainable but developing methods of tracing are still effective and are useful for both, users and developers [6][11][12]. Most of this research is done in image classification, so attempting to interpret the decision process of modern AI comes down to looking at the picture and what areas of the picture the AI is taking into consideration most. Preece describes this as post-hoc interpretations, which is analysis of the data after the data is seen. He further makes the point that true transparency is not quite the same as post-hoc interpretations but is often good enough [12]. 

\section{XAI in the Medical Domain}
Expert systems have been successful in the past when striving towards transparency in specific domains. However, it generally is less flexible and unable to learn on its own. Adadi claims in a survey of XAI that the wide-spread adoption of AI in all aspects of life has reopened the question of explainability [13]. The medical domain deals with people's lives and health. Wrong decisions are catastrophic. Doctors are trusted due to their expert knowledge, ability to reason out a diagnosis and plan accordingly. The goal of XAI in the medical domain is to not only come up with a diagnosis, but also be able to justify the diagnosis by showing the inferences made in the same way that a doctor would. To this end, there has been some success.

Classical AI is dominant in this domain for interacting with users. Thus research by Kass and Finin define what good explanations look like and the need for explanations to be catered to particular individuals due to the variety of knowledge levels of users [3]. Buchanan and Shortliffe introduce Mycin to personalize expert systems' explanations to users and developers through the use of meta-knowledge to guide the process of the construction of the explanations [4]. Swartout bulds upon the idea of meta-knowledge and introduces the separation of domain knowledge from algorithmic specific knowledge to improve developers' ability to debug while constructing informative explanations for regular users or experts. Most of these systems are used for consultation in the medical domain.

Progress in utilizing both, expert systems and modern AI techniques is being made with XAI. Chittajallu, Dong, Tunison, et al. presented the ability to generate visual explanations and feedback by utilizing a human-in-the-loop XAI system for minimally invasive surgery. In this case, the AI was comparing video frames to stored images and was able to generate a visual explanation as to why the two images matched based on relevance feedback from a doctor [14]. Long, Slagle, Wick, et al. and Kulikowski discuss models for expert systems for medical data analysis and decision making under uncertainty [15][16]. Adadi claims that XAI can also be used to augment human intelligence. Rather than rely on on a result, a doctor can utilize the predictive power of AI with techniques of explainability to find possibilities or suggestions [13].

\section{Improving XAI}
Modern AI is difficult to explain due to its design and emphasis on predictive power and little focus on explainability. New types of algorithms that are built for both, predictive power and the ability to show its reasoning are being researched. Research by Ding shows an approach derived from how humans obtain knowledge called Neural Logic Network (NLN) which uses weights to form a set of possible and flexible operations [17]. NLN's would be used in conjunction with other Modern AI approaches that rank better for explainability as ranked by D. Gunning [18]. Further XAI improvements could come from researching and formally defining terms that to reach a scientific consensus such as explainability and interpretability [12][19][20]. Other research is being done in cognitive science and how knowledge can be obtained is underway as well [21].

Preece researched an explainable machine learning framework based on ideas from classical AI approaches, namely from Buchanan and Shortliffe [4], to provide both transparency and post-hoc explanations, by utilizing meta and domain knowledge.

Xie, Chen and Gao laid the ground work for medical diagnosis AI by defining the user-centric perspective of the AI, similar to [21]. Formal research was done on how XAI systems should reason to match that of a doctors reasoning. Xie, Chen and Gao also claims that domain experts and cognitive scientists need to become more involved in how the reasoning should be performed in that particular domain [21]. XAI and human-computer interaction (HCI) for medical diagnosis is starting to emerge, but there still needs to be more research into new methods and domain experts need to be involved in what the reasoning should look like in order to quantify the efficacy of these new methods.

\section{Conclusion}
In order to further utilize AI in critical areas such as the medical field, humans need to be able to establish trust that the algorithm is getting the correct result. With modern AI, this has proven to be difficult. With classical AI it is trivial but it is also less flexible. Some hybrid approaches to XAI brings the best of modern and classical AI together in an attempt to have the algorithm explain its reasoning so that humans can understand. Modern methods have been ranked in accordance to ease of explainability and predictive power [18]. New techniques combined with modern AI is being researched to bring XAI into critical fields such as the medical field [17]. Cognitive scientists and domain experts are key if accurate models are to be obtained [21]. 

The consensus of the literature is that true transparency is difficult with modern AI due to the shift of focus from explainability to interpretability, the lack of transparency due to the numerical and non-symbolic nature of the algorithms, and difference in the types of interpretations needed for particular domains. Many researchers would go as far as to say that true transparency is unobtainable with modern AI. However, post-hoc interpretations have become popular for interpretation, especially with image classification. Classical AI, namely expert systems, is the dominant approach to medical consultation systems due to the ease of traceability. The literature shows that providing explanations that are catered towards a particular user group is important for explainability, whether it be doctors, regular users or developers. When it comes to expert systems, meta-knowledge is powerful approach to do this. It has been shown that combining methods from both classical AI and modern AI is productive. Finally, many researchers agree that more formal definitions are needed, especially for interpretability. Overall, more research needs to be done in XAI in general and the medical domain.
\newpage
\section*{References}
\addcontentsline{toc}{section}{References}%
\renewcommand{\section}[2]{}%
\begin{thebibliography}{1}
\singlespacing


\bibitem{Goebel} R. Goebel, A. Chander, K. Holzinger, F. Lecue, Z. Akata, et al.. Explainable AI: the new 42?. 2nd Int. Cross-Domain Conf. for Mach. Learning and Knowl. Extraction (CD-MAKE), Aug 2018, Hamburg, Germany. pp 295-303, ff10.1007/978-3-319-99740-7$\_$21ff. ffhal-01934928f \\


  \bibitem{Hoffman} R. R. Hoffman, G. Klein, and S. T. Mueller, "Explaining explanation for “explainable AI”" in  \textit{Sage Journals}, vol.62 no. 1, pp. 197-201, Sept. 2018. [Online]. doi:10.1177/1541931218621047\\
  
    \bibitem{Kass} R. Kass, and T. Finin, "The need for user models in generatnig exper system explanations" 1988. [Online]. https://repository.upenn.edu/cis/\\

    \bibitem{Buchanan} B. G. Buchanan, and E. H. Shortliffe, "Rule-based expert systems:
The MYCIN experiments of the Stanford heuristic programming project" pp. 754 1984. [Online]. http://www.shortliffe.net/Buchanan-Shortliffe-1984/MYCIN\\


    \bibitem{Swartout} W. Swartout, C. Paris, and J. Moore, "Explanations in knowledge systems: design for explainable expert systems".  \textit{IEEE Expert} pp. 58–64 1991. [Online]. http://people.dbmi.columbia.edu/\\

    \bibitem{Bratko} I. Bratko, "Machine learning: between accuracy and interpretability". in \textit{Int. Centre for Mech. Sci.} vol 382 pp. 163-167 1997. [Online]. https://link.springer.com/chapter/10.1007/978-3-7091-2668-4$\_$10\\



\bibitem{Lou} Y. Lou, R. Caruana, and J.Gehrke. "Intelligible models for classification and regression". in  \textit{Proc. 18th ACM SIGKDD Int. Conf.  Knowl. Discovery and Data Mining}, 2012, pages 150–158. ACM.

  \bibitem{Ribeiro} M. T. Ribeiro, S. Singh, C. Guestrin, ""Why should I trust you?": Explaining the predictions of any classifier" in \textit{IEEE Access}, vol.6,  pp. 52138 - 52160, Sept. 2018. [Online]. doi: 10.1109/ACCESS.2018.2870052\\
  
    \bibitem{Montavon} G. Montavon, S. Bach, A. Binder, W. Samek, K. Muller, "Explaining non-linear classification decisions with deep taylor decomposition" in \textit{Pattern
Recognition},  pp. 211–222, 2017. [Online]. https://arxiv.org/abs/1704.04133\\

    \bibitem{Kumar} D. Kumar, A. Wong, G. W.  Taylor, "Explaining the unexplained: A class-enhanced attentive response (CLEAR) approach to understanding deep neural networks"  [Online]. https://arxiv.org/abs/1602.04938\\
    
        \bibitem{Lipton} Z. C. Lipton, "The mythos of model interpretability" in \textit{ICML Workshop on Human Interpretability in Mach. Learning (WHI 2016)}, 2016. [Online]. https://arxiv.org/abs/1606.03490\\

    \bibitem{Preece} A. Preece, "Asking 'why' in AI: explainability of intelligent systems - perspectives and challenges". [Online]. https://dais-ita.org/sites/default/files/2326$\_$paper.pdf\\

  \bibitem{Peeked} A. Adadi, M. Berrada, "Peeking inside the black-box: a survey on explainable artificial intelligence" in \textit{Proc. of the 22nd ACM SIGKDD Int. Conf. on Knowl. Discovery and Data Mining (KDD’16),}, pages 1135–
1144. ACM. 2016. [Online].https://dais-ita.org/sites/default/files/2326$\_$paper.pdf\\
  
    \bibitem{Chittajallu} D. R. Chittajallu, B. Dong, P. Tunison, R. Collins, k. Wells, et al., "XAI-CBIR: explainable AI system for content based retrieval of video frames from minimally invasive surgery videos" in \textit{2019 IEEE 16th Int. Symposium on Biomed. Imaging (ISBI 2019)}, April. 2019. [Online]. doi: 10.1109/ISBI.2019.8759428\\
  
  
    \bibitem{Kulikowski} C. A. Kulikowski, "Artificial intelligence methods and systems
for medical consultation " in \textit{IEEE Trans. Pattern Anal.  Mach. Intell.}, vol. PAMI-2, no. 5 pp. 464 - 476, Sept. 1980. [Online]. https://ieeexplore.ieee.org/document/6592368\\
  
      \bibitem{Long} J. M. Long, j. R. Slagle, M. Wick, E. Irani, j. Matts, et al. "Use of expert systems in medical research data analysis: The POSCH AI project ", vol.1, pp. 796. [Online]. doi: 10.1109/AFIPS.1987.120\\
  
        \bibitem{Ding} L. Ding "Human knowledge in constructing AI systems - neural logic
networks approach towards an explainable AI ", \textit{Int. Conf. Knowl. Based and Intell. Inf. and Eng. Syst.}, Sept. 2018. [Online]. https://app.dimensions.ai/details/publication/pub.1106387404?and$\_$facet$\_$journal=jour.1320495\\
  
        \bibitem{gunning}D. Gunning "Explainable artificial intelligence",  in \textit{DARPA/120}, Nov. 2017, [Online]. https://www.darpa.mil/attachments/XAIProgramUpdate.pdf\\
  
  
          \bibitem{Tjoa} E. Tjoa and C. Guan "A survey on explainable artificial intelligence (XAI): towards medical XAI", July. 2019, [Online]. https://arxiv.org/abs/1907.07374\\
  
  
            \bibitem{Doshi-Velez} F. Doshi-Velez and B. Kim "Towards a rigorous science of interpretable machine learning", Fep. 2017, [Online]. https://arxiv.org/abs/1702.08608\\
  
              \bibitem{Xie} Y. Xie, X. Chen, and G.Gao "Outlining the design space of explainable intelligent systems for medical diagnosis", March. 2019, [Online]. https://arxiv.org/pdf/1902.06019.pdf\\
  
  

  \end{thebibliography}
\end{document}