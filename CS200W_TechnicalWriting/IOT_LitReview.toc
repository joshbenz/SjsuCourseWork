\contentsline {section}{\numberline {I. }Introduction}{1}% 
\contentsline {section}{\numberline {II. }Transparency and Artificial Intelligence}{2}% 
\contentsline {subsection}{\numberline {A.}Classical AI: User and Developer Perspectives}{2}% 
\contentsline {subsection}{\numberline {B.}Modern AI: User and Developer Perspectives}{3}% 
\contentsline {section}{\numberline {III. }XAI in the Medical Domain}{5}% 
\contentsline {section}{\numberline {IV. }Improving XAI}{5}% 
\contentsline {section}{\numberline {V. }Conclusion}{6}% 
\contentsline {section}{References}{8}% 
