/*
 * This is the main module and driver of the homework problems.
 * For problem 14, I used the cypher_crypt crate
 * (https://docs.rs/crate/cipher-crypt/0.18.0)
 * rather than write my own cipher.
 *
 * I also use threads_pool crate so I didn't need to setup and manage
 * my own threadpool. (https://docs.rs/crate/threads_pool/0.2.5)
 */

use cipher_crypt::{Caesar, Cipher}; //Caesar cipher is a simple alphabet shift
use std::collections::HashMap;
use std::env;
use std::sync::atomic::{AtomicU32, Ordering};

use std::sync::Arc;
use std::time::{Duration, Instant};
use threads_pool::*;

mod hmm;
mod mat;
use hmm::HMM;
use mat::Mat;

//returns the number correct and the discovered key
fn score_shift(key: String, b_matrix: Mat, pi: Mat) -> (u32, String) {
    let mut map = HashMap::new();
    let mut counter: usize = 0;
    let a = "abcdefghijklmnopqrstuvwxyz";

    for c in a.chars() {
        map.insert(counter, c.to_string());
        counter += 1 as usize;
    }
    let mut putative_key = String::new();
    let mut highest_pos: usize = 0;
    let mut highest_score: f64 = 0.0;

    //look at pi vector for a starting point
    let pi_dimensions = pi.sizes();
    for i in 0..pi_dimensions.1 {
        let val = pi.vec_get(i);
        if val > highest_score {
            highest_score = val;
            highest_pos = i;
        }
    }

    //use the highest pi
    let mut row_letter = map.get(&highest_pos).unwrap();
    let mut row = highest_pos;
    putative_key.push_str(row_letter);

    let dimensions = b_matrix.sizes();
    for i in 1..key.len() {
        highest_score = 0.0;
        highest_pos = 0;
        for next_letter in 0..dimensions.1 {
            let val = b_matrix.get(row, next_letter);
            if val > highest_score {
                if !putative_key.contains(map.get(&next_letter).unwrap()) {
                    highest_score = val;
                    highest_pos = next_letter;
                }
            }
        }
        //use the next best letter
        row_letter = map.get(&highest_pos).unwrap();
        row = highest_pos;
        putative_key.push_str(row_letter);
    }

    let mut correct: u32 = 0;
    let p_key: Vec<char> = putative_key.chars().collect();
    let correct_key: Vec<char> = key.chars().collect();

    for i in 0..p_key.len() {
        if (p_key[i] == correct_key[i]) {
            correct += 1;
        }
    }

    //let final_score = correct / (p_key.len() as f32);
    return (correct, putative_key);
}

fn crypto_problem(
    shift: usize,
    iterations: u32,
    n_models: usize,
    alphabet: String,
    text: &str,
    epsilon: f64,
    final_score_count: Arc<AtomicU32>,
) {
    let cipher = Caesar::new(shift); //shift alphabet by 3
    let key = cipher.encrypt("abcdefghijklmnopqrstuvwxyz").unwrap();
    let cipher_text = cipher.encrypt(text).unwrap();

    //setup threadpool
    let pool = ThreadPool::new(8);
    for i in 0..n_models {
        let obs = cipher_text.clone(); //making copies is just easier
        let alphabet_copy = alphabet.clone();
        let key_clone = key.clone();
        let final_score_count = Arc::clone(&final_score_count);

        pool.execute(move || {
            //create model and train it
            let mut model = HMM::from(26, &alphabet_copy, &obs, "BrownCorpus", 0 as usize);
            //model.baum_welch(iterations, epsilon); //0.00001);
            model.fixed_A_train(iterations, epsilon);
            let b_transpose = model.B().transpose();
            let pi = model.pi();

            let mut score = score_shift(key_clone, b_transpose, pi);

            let curr_score = final_score_count.load(Ordering::SeqCst);
            if curr_score < score.0 {
                final_score_count.store(score.0, Ordering::SeqCst);
                let f_score: f32 = score.0 as f32 / 26.0;
                println!(
                    "Higher Scoring model found! Key: {}, Score: {}",
                    score.1, f_score
                );
            }

            println!("key: {}, score: {}", score.1, score.0 as f32 / 26.0);
        });
        println!("Model: {} Queued", i);
    }
}

fn zodiac_score(key: String, b_matrix: Mat, pi: Mat) -> (u32, String) {
    let mut putative_key = String::new();
    let mut highest_pos: usize = 0;
    let mut highest_score: f64 = 0.0;

    let mut map = HashMap::new();
    let mut counter: usize = 0;

    for c in key.split_whitespace() {
        map.insert(counter, c.to_string());
        counter += 1 as usize;
    }

    //look at pi vector for a starting point
    let pi_dimensions = pi.sizes();
    for i in 0..pi_dimensions.1 {
        let val = pi.vec_get(i);
        if val > highest_score {
            highest_score = val;
            highest_pos = i;
        }
    }

    //use the highest pi
    let mut row_letter = map.get(&highest_pos).unwrap();
    let mut row = highest_pos;
    putative_key.push_str(row_letter);
    putative_key.push_str(" ");

    let dimensions = b_matrix.sizes();
    for i in 1..map.len() {
        highest_score = 0.0;
        highest_pos = 0;
        for next_letter in 0..dimensions.1 {
            let val = b_matrix.get(row, next_letter);
            if val > highest_score {
                if !putative_key.contains(map.get(&next_letter).unwrap()) {
                    highest_score = val;
                    highest_pos = next_letter;
                }
            }
        }
        //use the next best letter
        row_letter = map.get(&highest_pos).unwrap();
        row = highest_pos;
        putative_key.push_str(row_letter);
        putative_key.push_str(" ");
    }

    let mut correct: u32 = 0;
    let p_key: Vec<char> = putative_key.chars().collect();
    let correct_key: Vec<char> = key.chars().collect();

    for i in 0..p_key.len() {
        if (p_key[i] == correct_key[i]) {
            correct += 1;
        }
    }

    //let final_score = correct / (p_key.len() as f32);
    return (correct, putative_key);
}

fn zodiac_problem(
    iterations: u32,
    n_models: usize,
    alphabet: String,
    text: &String,
    epsilon: f64,
    final_score_count: Arc<AtomicU32>,
) {
    let pool = ThreadPool::new(8);
    for i in 0..n_models {
        let obs = text.clone(); //making copies is just easier
        let alphabet_copy = alphabet.clone();
        let key_clone = alphabet.clone();
        let final_score_count = Arc::clone(&final_score_count);

        pool.execute(move || {
            //create model and train it
            let mut model = HMM::from(26, &alphabet_copy, &obs, "BrownCorpus", 0 as usize);
            //model.baum_welch(iterations, epsilon); //0.00001);
            model.fixed_A_train(iterations, epsilon);
            let b_transpose = model.B().transpose();
            let pi = model.pi();

            let mut score = zodiac_score(key_clone, b_transpose, pi);

            let curr_score = final_score_count.load(Ordering::SeqCst);
            if curr_score < score.0 {
                final_score_count.store(score.0, Ordering::SeqCst);
                let f_score: f32 = score.0 as f32 / 26.0;
                println!(
                    "Higher Scoring model found! Key: {}, Score: {}",
                    score.1, f_score
                );
            }

            println!("key: {}, score: {}", score.1, score.0 as f32 / 26.0);
        });
        println!("Model: {} Queued", i);
    }
}

/*ARGS
 * 0: iterations u32
 * 1: observation size u32
 * 2: number of models usize
 *
 */

fn main() {
    let alphabet = String::from("abcdefghijklmnopqrstuvwxyz");
    //input.chars().take(250).collect()
    let args: Vec<String> = env::args().collect();
    let text = String::from("In every discourse whether of the mind conversing with its own thoughts or of the individual in his intercourse with others there is an assumed or expressed limit within which the subjects of its operation are confined The most unfettered discourse is that in which the words we use are understood in the widest possible application and for them the limits of discourse are coextensive with those of the universe itself But more usually we confine ourselves to a less spacious field Sometimes in discoursing of men we imply without expressing the limitation that it is of men only under certain circumstances and conditions that we speak as of civilised men or of men in the vigour of life or of men under some other condition or relation Now whatever may be the extent of the field within which all the objects of our discourse are found that field may properly be termed the universe of discourse Furthermore this universe of discourse is in the strictest sense the ultimate subject of the discourse");

    let iterations: u32 = args[1].parse().unwrap();
    let obs_size: usize = args[2].parse().unwrap(); //who needs error handling..am i right??
    let n_models: usize = args[3].parse().unwrap();

    println!(
        "Parameters:\n Iterations: {}\n Observation Sequence Lenght: {}\n Number of Models: {}\n",
        iterations, obs_size, n_models
    );

    let text: String = text.chars().take(obs_size).collect();
    //for some reason Rust doesn't have an AtomicF32 yet, so I will
    //use a u32 score of correct symbols and divide the result
    //at the end of execution
    let crypto_score = Arc::new(AtomicU32::new(0));
    crypto_problem(
        3, //shift
        iterations,
        n_models,
        alphabet,
        text.as_str(),
        0.00001, //epsilon
        Arc::clone(&crypto_score),
    );

    let zodiac = String::from(
        "0 1 2 3 4 3 5 6 1 7 8 9 10 11 12 10 6
13 14 15 16 17 18 19 20 0 21 2 22 23 24 25 18 16
26 27 18 28 5 29 7 30 25 31 32 33 34 18 35 36 37
38 39 3 0 40 6 2 8 9 41 5 1 42 9 43 25 44
7 28 45 26 4 27 46 47 48 11 19 21 14 13 16 49 18
22 15 25 17 35 0 23 29 37 20 25 12 49 36 50 38 39
9 33 32 29 18 44 43 8 49 25 17 6 31 34 38 40 6
45 8 3 2 40 6 22 12 25 44 21 26 5 28 9 9 7
51 4 23 25 11 28 37 13 25 29 49 36 45 26 47 0 40
6 2 35 9 15 53 10 20 48 33 39 16 44 5 21 7 19
4 51 11 8 14 13 29 36 15 324537 43 28 9 20 21
29 0 35 9 52 31 18 47 48 46 16 3 22 12 27 34 41
2 36 26 49 9 5 32 1 45 37 33 14 44 23 21 10 17
47 29 29 27 7 36 0 49 45 26 43 33 41 37 4 39 2
50 5 11 7 41 0 40 6 14 13 48 15 14 31 32 8 2
28 10 38 47 43 42 5 16 20 30 35 50 17 1 1 29 26
33 7 37 38 51 44 3 0 1 1 4 42 41 2 40 6 14
11 16 12 25 13 25 52 19 40 49 51 15 22 0 41 0 6
1 8 31 36 9 5 51 15 52 46 18 25 52 28 38 25 13
14 4 16 17 17 23 44 52 31 18 41 0 1 40 45 32 52
21 29 19 6 12 0 50 12 41 35 46 48 30 45 29 10 25
52 16 46 40 40 20 16 36 2 8 9 12 34 19 1 17 51
4 22 27 31 32 25 52 49 27 29 15 47 6 2 34 13 20
14 44 12 47 0 13 29 20 25 44 21 26 37 10 5 29 7",
    );

    let mut zodiac_alphabet = String::new();
    for i in 0..54 {
        let tmp = String::from(i.to_string().as_str());
        zodiac_alphabet.push_str(tmp.as_str());
        zodiac_alphabet.push_str(" ");
    }
    /*zodiac_problem(
        iterations,
        n_models,
        zodiac_alphabet,
        &zodiac,
        0.00001,
        Arc::clone(&crypto_score),
    );*/
}
