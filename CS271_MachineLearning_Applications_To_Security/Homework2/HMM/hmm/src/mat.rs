/*
 * This Module encapsulates a Matrix as a 2D vector (A vector of vectors of floats)
 * By using with_capacity, i garuntee that there will be no reallcations
 * unless I step over the capacity of the vector. To avoid the posibility
 * of this even further, I only pass mutable refernces of fixed size slices
 * to various functions.
 *
 * Elements in the matrix are randomly generate and row stochastic
 * form is ensured in this module. See Function comments for more info.
 *
 * This module also contains the code for generating the digraph frequency
 * matrix.
 */

use rand::Rng;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

pub type Matrix = Vec<Vec<f64>>;
pub type Vector = Vec<f64>;
pub type VectorSlice = [f64];

pub struct Mat {
    matrix: Matrix,
    N: usize,
    M: usize,
}

impl Mat {
    pub fn new(N: usize, M: usize) -> Mat {
        //with_capacity garuntees that there will be no
        //reallocations until I step over the specified capacity
        let mut vec: Matrix = Vec::with_capacity(N);
        for i in 0..N {
            let mut vec_sum_tuple: (Vector, f64) = Mat::random_vec(M);
            Mat::row_stochastic_form(vec_sum_tuple.0.as_mut_slice(), vec_sum_tuple.1);
            vec.push(vec_sum_tuple.0);
        }

        Mat {
            matrix: vec,
            N: N,
            M: M,
        }
    }

    pub fn sizes(&self) -> (usize, usize) {
        return (self.N, self.M);
    }
    pub fn empty(N: usize, M: usize) -> Mat {
        let mut vec: Matrix = Vec::with_capacity(N);
        for i in 0..N {
            let v = vec![0.0; M]; //similar to with_capacity, but fills with 0s
            vec.push(v);
        }

        Mat {
            matrix: vec,
            N: N,
            M: M,
        }
    }

    pub fn di_graph_freq(filename: &str, offset: usize) -> Mat {
        //converting between u32 and usize is really annoying so this
        //map sould make things easier
        let mut alphabet = HashMap::new();
        let a = "abcdefghijklmnopqrstuvwxyz";
        let mut counter: usize = 0;

        for c in a.chars() {
            alphabet.insert(c.to_string(), counter);
            counter += 1 as usize;
        }

        let mut counter = 0;
        let mut freq_matrix: Mat = Mat::empty(26, 26);

        //parse the brown corpus
        let path = Path::new(filename);
        let file = match File::open(&path) {
            Ok(file) => file,
            Err(e) => panic!("It's broken i guess"),
        };

        let reader = BufReader::new(file);
        for line in reader.lines() {
            match line {
                Ok(line) => {
                    //I have a line
                    //skip the useless columns
                    let char_vec: Vec<char> = line.chars().skip(offset).collect();

                    let n = char_vec.len();
                    for i in 0..n {
                        if i + 1 < n {
                            let first_char = char_vec[i];
                            let second_char = char_vec[i + 1 as usize];

                            if first_char.is_alphabetic() && second_char.is_alphabetic() {
                                let row: usize = *alphabet
                                    .get(&first_char.to_lowercase().to_string())
                                    .unwrap();
                                let col: usize = *alphabet
                                    .get(&second_char.to_lowercase().to_string())
                                    .unwrap();
                                let mut sum = freq_matrix.get(row, col);
                                freq_matrix.set(row, col, sum + 1.0);
                            }
                        }
                    }
                }
                Err(e) => println!("ERROR: {}", e),
            }
        }
        //normalize it
        for i in 0..freq_matrix.N {
            let mut sum = 0.0;
            for j in 0..freq_matrix.M {
                //add 5 to each
                let mut tmp = freq_matrix.get(i, j);
                tmp += 5.0;
                freq_matrix.set(i, j, tmp);

                //for nomralizing
                sum += freq_matrix.get(i, j);
            }

            //divide each by sum
            for j in 0..freq_matrix.M {
                let tmp = freq_matrix.get(i, j);
                let norm = tmp / sum;
                freq_matrix.set(i, j, norm);
            }
        }
        return freq_matrix;
    }

    pub fn transpose(&self) -> Mat {
        let mut matrix: Mat = Mat::empty(self.M, self.N);

        for i in 0..self.N {
            for j in 0..self.M {
                matrix.set(j, i, self.matrix[i][j]);
            }
        }
        return matrix;
    }

    /* given a mutable ref to a vec row, make it row stochastic.
     * This will be done similar to here https://www.rdocumentation.org/packages/sna/versions/2.5/topics/make.stochastic
     */
    fn row_stochastic_form(vec: &mut VectorSlice, sum: f64) {
        let mut vec_sum = 0.0;
        for i in 0..vec.len() {
            vec[i] = vec[i] / sum;
            vec[i] = (vec[i] * 1000000.0).round() / 1000000.0;
            vec_sum += vec[i];
        }

        /* At this point the sum is 1.0 or very close to 1.0
         * If they do not add up to 1.0 exactly, I will take
         * what is needed and add it to one of the elements
         * Or take away. I suspect there are some floating point
         * arithmatic issues as well.
         */
        let mut test = 0.0;
        if vec_sum < 1.0 {
            let diff = vec_sum - 1.0;
            vec[0] -= diff;
        } else if vec_sum > 1.0 {
            let diff = 1.0 - vec_sum;
            vec[0] += diff;
        } else {
            //do nothing since it is equal to 1.0
        }
    }

    //randomly fills a vector with values,
    //returns the vector and what it sums to
    fn random_vec(size: usize) -> (Vector, f64) {
        let mut rng = rand::thread_rng(); //gets seeded by the system
        let mut vec = Vec::with_capacity(size);
        let mut sum: f64 = 0.0;

        for i in 0..size {
            let rand_n = rng.gen::<u32>(); //usinged to avoid negative numbers
            sum += rand_n as f64;
            vec.push(rand_n as f64);
        }

        return (vec, sum);
    }

    pub fn print(&self) {
        for i in 0..self.N {
            for j in 0..self.M {
                print!("{:.5}, ", self.matrix[i][j]);
            }
            println!("");
        }
    }

    pub fn get(&self, i: usize, j: usize) -> f64 {
        self.matrix[i][j]
    }

    pub fn set(&mut self, i: usize, j: usize, val: f64) {
        self.matrix[i][j] = val;
    }

    //for Mat::Vector since I like to slightly waste memory
    //Rust doesn't support function overloading
    pub fn vec_get(&self, i: usize) -> f64 {
        self.matrix[0][i]
    }

    pub fn vec_set(&mut self, i: usize, val: f64) {
        self.matrix[0][i] = val;
    }
}
