/*
 * This is the main module and driver of the homework problems.
 * For problem 14, I used the cypher_crypt crate
 * (https://docs.rs/crate/cipher-crypt/0.18.0)
 * rather than write my own cipher.
 *
 * I also use threads_pool crate so I didn't need to setup and manage
 * my own threadpool. (https://docs.rs/crate/threads_pool/0.2.5)
 */

use std::env;
mod hmm;
mod mat;
use hmm::HMM;
use mat::Mat;

/*ARGS
 * 0: iterations u32
 * 1: observation size u32
 * 2: number of models usize
 *
 */

fn main() {
    let alphabet = String::from("abcdefghijklmnopqrstuvwxyz");
    //input.chars().take(250).collect()
    let args: Vec<String> = env::args().collect();
    let iterations: u32 = args[1].parse().unwrap();
    let obs_size: usize = args[2].parse().unwrap(); //who needs error handling..am i right??
    let n_models: usize = args[3].parse().unwrap();

    let mut model = HMM::from(2 as usize, &alphabet, "BrownCorpus", 0 as usize);
    model.print_results();

    println!(
        "Parameters:\n Iterations: {}\n Observation Sequence Lenght: {}\n Number of Models: {}\n",
        iterations, obs_size, n_models
    );
}
