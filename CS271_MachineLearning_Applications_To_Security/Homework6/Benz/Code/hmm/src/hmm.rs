/*
 * This module encapsulates everything needed to run the Hidden Markov Model
 * I use the Matrices defined in Mat.rs which takes care of allocating space
 * as well as ensurng they are random and row stochastic.
 *
 * This module is responsible for allocating memory for the HMM given
 * a size and an observation string. It will parse the observation string
 * into a vector of observation sequences. The Observation struct holds
 * the alphas, betas, gammas and di-gammas for each observation. I then
 * provide a means to train the model given the number of iterations and
 * an epsilon for measurement of improvement.
 */

use super::mat::Mat;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Patch;

pub struct HMM {
    A: Mat,
    B: Mat,
    pi: Mat,
    observation_sequence: Vec<Observation>,
    N: usize, //number of hidden states
    M: usize, //number of distinct observation symbols
}

impl HMM {
    //N: number of hidden states
    //V: the vocabulary aka set of possible observation symbols
    // used to find M and create Observations
    //s: input string to create the observation sequence
    pub fn new(N: usize, V: &String, s: &String) -> HMM {
        let mut obs_seq = Vec::with_capacity(s.len());

        //parse the observation string into a vector of
        //observations. Only characters defined in the
        //vocabuary V are allowed to be observations.
        for obs in s.chars() {
            let obs = obs.to_lowercase().to_string();
            for (i, c) in V.chars().enumerate() {
                if c.to_string() == obs {
                    //we have a legal char
                    obs_seq.push(Observation::new(N, i));
                }
            }
        }

        //return the object
        HMM {
            A: Mat::new(N, N),
            B: Mat::new(N, V.len()),
            pi: Mat::new(1, N),
            observation_sequence: obs_seq,
            N: N,
            M: V.len(),
        }
    }

    pub fn B(&self) -> Mat {
        let dimensions = self.B.sizes();
        let mut b = Mat::empty(dimensions.0, dimensions.1);

        for i in 0..dimensions.0 {
            for j in 0..dimensions.1 {
                let val = self.B.get(i, j);
                b.set(i, j, val);
            }
        }
        return b;
    }

    pub fn pi(&self) -> Mat {
        let dimensions = self.pi.sizes();
        let mut pi = Mat::empty(dimensions.0, dimensions.1);

        for i in 0..dimensions.0 {
            for j in 0..dimensions.1 {
                let val = self.pi.vec_get(j);
                pi.vec_set(j, val);
            }
        }
        return pi;
    }

    pub fn from(N: usize, V: &String, corpus: &str, offset: usize) -> HMM {
        let mut obs_seq = Vec::with_capacity(2);

        //parse the observation string into a vector of
        //observations. Only characters defined in the
        //vocabuary V are allowed to be observations.
        /*for obs in s.chars() {
            let obs = obs.to_lowercase().to_string();
            for (i, c) in V.chars().enumerate() {
                if c.to_string() == obs {
                    //we have a legal char
                    obs_seq.push(Observation::new(N, i));
                }
            }
        }*/

        //return the object
        HMM {
            A: Mat::new(N, N),
            B: Mat::new(V.len(), N),
            pi: Mat::new(1, N),
            observation_sequence: obs_seq,
            N: N,
            M: V.len(),
        }
    }

    pub fn from_corpus(N: usize, V:&String, corpus: &str) -> {

    }

    pub fn from_zodiac(N: usize, V: &String, s: &String, corpus: &str, offset: usize) -> HMM {
        let mut obs_seq = Vec::with_capacity(0);

        for obs in s.split_whitespace() {
            for (i, v) in V.split_whitespace().enumerate() {
                obs_seq.push(Observation::new(N, i));
            }
        }

        //return the object
        HMM {
            A: Mat::new(N, V.len()),
            B: Mat::new(N, V.len()),
            pi: Mat::new(1, N),
            observation_sequence: obs_seq,
            N: N,
            M: V.len(),
        }
    }

    pub fn baum_welch(&mut self, iterations: u32, epsilon: f64) {
        let mut curr_log_prob: f64 = -1.0; //negative infinity
        let mut log_prob: f64 = 0.0;
        let mut delta: f64 = 0.0;

        for i in 0..iterations {
            self.alpha_pass();
            self.beta_pass();
            self.gammas();

            self.reestimate_pi();
            self.reestimate_A();
            self.reestimate_B();

            log_prob = self.log();
            delta = (curr_log_prob - log_prob).abs();
            if delta > epsilon {
                curr_log_prob = log_prob;
            } else {
                println!("A model Finished");
                return; //no more progress, the model is done
            }
        }
    }

    pub fn fixed_A_train(&mut self, iterations: u32, epsilon: f64) {
        let mut curr_log_prob: f64 = -1.0; //negative infinity
        let mut log_prob: f64 = 0.0;
        let mut delta: f64 = 0.0;

        for i in 0..iterations {
            self.alpha_pass();
            self.beta_pass();
            self.gammas();

            self.reestimate_pi();
            self.reestimate_B();

            log_prob = self.log();
            delta = (curr_log_prob - log_prob).abs();
            if delta > epsilon {
                curr_log_prob = log_prob;
            } else {
                println!("A model Finished");
                return; //no more progress, the model is done
            }
        }
    }

    fn alpha_pass(&mut self) {
        //alpha[0]'s
        let mut partial_sum: f64 = 0.0;
        for i in 0..self.N {
            self.observation_sequence[0].alpha[i] =
                self.pi.vec_get(i) * self.B.get(i, self.observation_sequence[0].alphabet_pos);
            partial_sum += self.observation_sequence[0].alpha[i];
        }
        self.observation_sequence[0].c = 1.0 / partial_sum;

        //scale alpha[0]'s
        for i in 0..self.N {
            self.observation_sequence[0].alpha[i] /= partial_sum;
        }

        let T = self.observation_sequence.len();
        //start at 1 since we did 0 above
        for t in 1..T {
            partial_sum = 0.0;
            for i in 0..self.N {
                self.observation_sequence[t].alpha[i] = 0.0; //reset from old alpa pass pr else tends to NaN
                for j in 0..self.N {
                    self.observation_sequence[t].alpha[i] += self.observation_sequence[t - 1].alpha
                        [j]
                        * self.A.get(j as usize, i as usize);
                }
                self.observation_sequence[t].alpha[i] *= self
                    .B
                    .get(i as usize, self.observation_sequence[t].alphabet_pos);
                partial_sum += self.observation_sequence[t].alpha[i];
            }
            self.observation_sequence[t].c = 1.0 / partial_sum;

            for i in 0..self.N {
                self.observation_sequence[t].alpha[i] /= partial_sum;
            }
        }
    }

    fn beta_pass(&mut self) {
        //compute beta T-1's
        let T = self.observation_sequence.len();

        for i in 0..self.N {
            self.observation_sequence[T - 1].beta[i] = 1.0 * self.observation_sequence[T - 1].c;
        }

        //kind of stupud notiation. (T-1, 0]
        //TODO: double check this range
        for t in (0..T - 1).rev() {
            for i in 0..self.N {
                self.observation_sequence[t].beta[i] = 0.0; //also reset otherwise tend to NaN
                for j in 0..self.N {
                    self.observation_sequence[t].beta[i] += self.A.get(i, j)
                        * self.B.get(j, self.observation_sequence[t + 1].alphabet_pos)
                        * self.observation_sequence[t + 1].beta[j];
                }
                self.observation_sequence[t].beta[i] *= self.observation_sequence[t].c;
            }
        }
    }

    fn gammas(&mut self) {
        let T = self.observation_sequence.len();

        for t in 0..T - 1 {
            for i in 0..self.N {
                self.observation_sequence[t].gamma[i] = 0.0; //reset from old gammas
                for j in 0..self.N {
                    let val = self.observation_sequence[t].alpha[i]
                        * self.A.get(i, j)
                        * self.B.get(j, self.observation_sequence[t + 1].alphabet_pos)
                        * self.observation_sequence[t + 1].beta[j];
                    self.observation_sequence[t].di_gamma.set(i, j, val);

                    self.observation_sequence[t].gamma[i] +=
                        self.observation_sequence[t].di_gamma.get(i, j);
                }
            }
        }

        //t = T-1
        for i in 0..self.N {
            self.observation_sequence[T - 1].gamma[i] = self.observation_sequence[T - 1].alpha[i];
        }
    }

    fn reestimate_pi(&mut self) {
        for i in 0..self.N {
            self.pi.vec_set(i, self.observation_sequence[0].gamma[i]);
        }
    }

    fn reestimate_A(&mut self) {
        let T = self.observation_sequence.len();

        for i in 0..self.N {
            let mut denom: f64 = 0.0;
            for t in 0..T - 1 {
                denom += self.observation_sequence[t].gamma[i];
            }

            for j in 0..self.N {
                let mut numer: f64 = 0.0;
                for t in 0..T - 1 {
                    numer += self.observation_sequence[t].di_gamma.get(i, j);
                }

                self.A.set(i, j, numer / denom);
            }
        }
    }

    fn reestimate_B(&mut self) {
        let T = self.observation_sequence.len();

        for i in 0..self.N {
            let mut denom: f64 = 0.0;
            for t in 0..T {
                denom += self.observation_sequence[t].gamma[i];
            }

            for j in 0..self.M {
                let mut numer: f64 = 0.0;
                for t in 0..T {
                    if self.observation_sequence[t].alphabet_pos == j {
                        numer += self.observation_sequence[t].gamma[i];
                    }
                }

                self.B.set(i, j, numer / denom);
            }
        }
    }

    fn log(&mut self) -> f64 {
        let T = self.observation_sequence.len();
        let mut log_prob: f64 = 0.0;

        for t in 0..T {
            log_prob += self.observation_sequence[t].c.log10(); //just use base 10
        }
        return -log_prob;
    }

    pub fn print_results(&self) {
        /*for i in 0..self.observation_sequence.len() {
            self.observation_sequence[i].print();
        }*/
        println!("PI: ");
        self.pi.print();
        println!("A: ");
        self.A.print();
        println!("B: ");
        self.B.print()
    }
}

//inspired by the C code from class
pub struct Observation {
    alphabet_pos: usize,
    c: f64,
    alpha: Vec<f64>,
    beta: Vec<f64>,
    gamma: Vec<f64>,
    di_gamma: Mat,
}

impl Observation {
    pub fn new(N: usize, pos: usize) -> Observation {
        Observation {
            alphabet_pos: pos,
            c: 0.0,
            alpha: vec![0.0; N],
            beta: vec![0.0; N],
            gamma: vec![0.0; N],
            di_gamma: Mat::empty(N, N),
        }
    }

    pub fn print(&self) {
        println!("alphas: {:?}\n", self.alpha);
    }
}
