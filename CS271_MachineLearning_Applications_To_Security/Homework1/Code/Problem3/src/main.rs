use std::env;
use std::fs;

//Globals with static lifetimes
//I will stick to the Notation for naming for now
static A: &'static [[f64; 2]; 2] = &[
    [0.7, 0.3], //State Transition probability Matrix
    [0.4, 0.6],
];

static B: &'static [[f64; 3]; 2] = &[
    [0.1, 0.4, 0.5], //Observation probability matrix
    [0.7, 0.2, 0.1],
];

static pi: &'static [f64] = &[0.6, 0.4]; //initial state distrobution

static hidden_state_combos: &'static [[u32; 4]; 16] = &[
    [0, 0, 0, 0],
    [0, 0, 0, 1],
    [0, 0, 1, 0],
    [0, 0, 1, 1],
    [0, 1, 0, 0],
    [0, 1, 0, 1],
    [0, 1, 1, 0],
    [0, 1, 1, 1],
    [1, 0, 0, 0],
    [1, 0, 0, 1],
    [1, 0, 1, 0],
    [1, 0, 1, 1],
    [1, 1, 0, 0],
    [1, 1, 0, 1],
    [1, 1, 1, 0],
    [1, 1, 1, 1],
];

fn compute(seq: Vec<u32>) -> f64 {
    let mut sum = 0.0;
    //for each hidden state sequence
    for test in hidden_state_combos {
        //calculate the probability and sum it for each hidden state sequence
        sum = sum
            + (pi[test[0] as usize]
                * B[test[0] as usize][seq[0] as usize]
                * A[test[0] as usize][test[1] as usize]
                * B[test[1] as usize][seq[1] as usize]
                * A[test[1] as usize][test[2] as usize]
                * B[test[2] as usize][seq[2] as usize]
                * A[test[2] as usize][test[3] as usize]
                * B[test[3] as usize][seq[3] as usize]);
    }
    return sum;
}

fn alpha_pass(seq: Vec<u32>) -> f64 {
    //we will have two vectors, one for each state. 


}

fn main() {
    //read the Observation permutations with repitition from a file
    let contents = //read it into a string
        fs::read_to_string("./output.txt").expect("Something went wrong reading the file");

    //split by newline and collect the strings into an vector
    let combos = contents.split_whitespace().collect::<Vec<&str>>();
    let mut sum = 0.0;

    //for each string in the combos vector
    for v in combos {
        //look at each character, convert it to an ASCII digit and collect the results into
        ////it's own vector. So I take string 0000 and convert it into vector [0,0,0,0]
        let vec: Vec<u32> = v.chars().filter_map(|c| c.to_digit(10)).collect();
        //This sequence is the observation sequence. Give compute() a copy of the vector
        let probability = compute(vec.clone());
        println!("P{:?} = {:?}", vec, probability);
        sum += probability; //add up to hopefull ad up to 1.
    }
    println!("The Sum: {:?}", sum);


    //part B of problem 3
    for v in combos {
        //look at each character, convert it to an ASCII digit and collect the results into
        ////it's own vector. So I take string 0000 and convert it into vector [0,0,0,0]
        let vec: Vec<u32> = v.chars().filter_map(|c| c.to_digit(10)).collect();
        let alpha = alpha_pass(vec.clone())
        //let probability = compute(vec.clone());
        //println!("P{:?} = {:?}", vec, probability);
        //sum += probability; //add up to hopefull ad up to 1.
    }
}
