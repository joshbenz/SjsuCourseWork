library(MASS)

score <- function(Y, U, delta, means) {
  #calculate the values for W
  W <- vector()
  for( i in 1:ncol(U)) {
    W <- c(W, Y %*% U[ , c(i)])
  }

  #score against delta
  min_so_far <- dist(rbind(W, delta[ , c(1)]))

  for(i in 1:ncol(delta)) {
    distance <- dist(rbind(W, delta[ , c(i)]))
    print(paste("Score: ", i, " ", distance))
    if(distance < min_so_far) {
      min_so_far <- distance
    }
  }
  return(min_so_far)
}


mat.means <- function(A) {
  means <- vector()
  sum = 0

  for(r in 1:nrow(A)) {
    for(c in 1:ncol(A)) {
      sum <- sum + A[r, c]
    }
    avg <- sum / ncol(A)
    means <- c(means, avg)
    sum <- 0
  }
  return(means)
}

mat.mean_norm <- function(A) {

  sum = 0
  for(r in 1:nrow(A)){
    for(c in 1:ncol(A)){
        sum = sum + A[r,c]
    }
    avg = sum / ncol(A)

    for(c in 1:ncol(A)) {
        A[r,c] = A[r,c] - avg
    }
    sum = 0
  }
  return(A)
}

mat.covar <- function(A) {
  return(((1/ncol(A)) * A) %*% t(A))
}

mat.delta <- function(A, U) {
   return(t(U) %*% A)
}

mat.eigenvectors <- function(C) {
  E <- eigen(C)
  return(E[["vectors"]])
}


  A <- matrix( 
   c(2, -2, -1, 3, 
    -1, 3, 3, -1,
    0, 2, 3, 0,
    1, 3, 1, 3,
    1, 0, -1, 2,
    -3, 2, 4, -1,
    5, -1, 5, 3,
    2, 1, 2, 0),  
   nrow=8,               
   ncol=4,               
   byrow = TRUE)  

#normalize by subtracting means
A <- mat.mean_norm(A)

# computer covariance matrix
C <- mat.covar(A)

# calculate eigenvalues and eigenvectors from C
U <- mat.eigenvectors(C)

# the frist 3 are the most significant
U <- U[ , c(1, 2, 3)]

#calculate delta
delta <- mat.delta(A, U)

#Y_1
print("Case 1:")
Y <- c(1, 5, 1, 5, 5, 1, 1, 3)
print(paste("Min Score:", score(Y, U, delta)))

#Y_2
print("Case 2:")
Y <- c(-2, 3, 2, 3, 0, 2, -1, 1)
print(paste("Min Score:", score(Y, U, delta)))

#Y_3
print("Case 3:")
Y <- c(2, -3, 2, 3, 0, 0, 2, -1)
print(paste("Min Score:", score(Y, U, delta)))

#Y_4
print("Case 4:")
Y <- c(2, -2, 2, 2, -1, 1, 2, 2)
print(paste("Min Score:", score(Y, U, delta)))
