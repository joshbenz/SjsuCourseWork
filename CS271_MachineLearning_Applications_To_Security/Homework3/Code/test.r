library(MASS)

score <- function(Y, U, delta) {
  #calculate the values for W
  W <- vector()
  for(c in 1:ncol(U)) {
    W <- c(W, Y %*% U[ , c(c)])
  }

  #score against delta
  min_so_far <- dist(rbind(W, delta[ , c(1)]))

  for(i in 1:ncol(delta)) {
    distance <- dist(rbind(W, delta[ , c(i)]))
    print(paste("Score: ", i, " ", distance))
    if(distance < min_so_far) {
      min_so_far <- distance
    }
  }
  return(min_so_far)
}

mean_normalize <- function(A) {

for(r in 1:nrow(A)){
    for(c in 1:ncol(A)){
        sum = sum + A[r,c]
    }
    avg = sum / ncol(A)
    means <- c(means, avg)
    
    for(c in 1:ncol(A)) {
        A[r,c] = A[r,c] - avg
    }
    sum = 0
  }
}

  A <- matrix( 
   c(2, 2, 1, 2,
     1, 3, 0, 3,
     0, 1, 3, 1,
     3, 2, 3, 0, 
     1, 3, 1, 3,
     1, 0, 1, 2),  
   nrow=6,               
   ncol=4,               
   byrow = TRUE)  


means <- vector()
sum = 0
for(r in 1:nrow(A)){
    for(c in 1:ncol(A)){
        sum = sum + A[r,c]
    }
    avg = sum / ncol(A)
    means <- c(means, avg)
    
    for(c in 1:ncol(A)) {
        A[r,c] = A[r,c] - avg
    }
    sum = 0
}
#print(A)
#print(svd(A))
#print(eigen(A))
C <- ((1/4) * A) %*% t(A)
E <- eigen(C)
U <- E[["vectors"]]
U <- U[ , c(1,2)]

delta <- t(U) %*% A
#print(delta)

Y <- c(2, 1, 0, 3, 1, 1)
Y <- Y - means

min_score <- score(Y, U, delta)
print(min_score)

