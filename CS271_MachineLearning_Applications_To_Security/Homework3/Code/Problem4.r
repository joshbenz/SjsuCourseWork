library(MASS)

  A <- matrix( 
   c(1, 2,
     3, 1,
     2, 3),  
   nrow=3,               
   ncol=2,               
   byrow = TRUE)  


means <- vector()
sum = 0
for(r in 1:nrow(A)){
    for(c in 1:ncol(A)){
        sum = sum + A[r,c]
    }
    avg = sum / ncol(A)
    means <- c(means, avg)
    
    for(c in 1:ncol(A)) {
        A[r,c] = A[r,c] - avg
    }
    sum = 0
}
#print(A)
#print(svd(A))
#print(eigen(A))
C <- ((1/2) * A) %*% t(A)
E <- eigen(C)

print(E)
