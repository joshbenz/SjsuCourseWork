library(MASS)

score <- function(W, delta, means) {
  #score against delta
  min_so_far <- dist(rbind(W, delta[ , c(1)]))

  for(i in 1:ncol(delta)) {
    distance <- dist(rbind(W, delta[ , c(i)]))
    #print(paste("Score: ", i, " ", distance))
    if(distance < min_so_far) {
      min_so_far <- distance
    }
  }
  return(min_so_far)
}

classify <- function(W, malware, benign, means) {
  malware_score <- score(W, malware, means)
  benign_score <- score(W, benign, means)

  if(malware_score < benign_score) {
    return(paste("Classified as Malware"))
  } 
  if(malware_score > benign_score) {
    return(paste("Classified as Benign"))
  }
  return(paste("Could not be classified"))
}

weight_vector <- function(Y, U, means) {
  Y <- Y - means
  W <- vector()
  for(i in 1:ncol(U)) {
    W <- c(W, Y %*% U[ , c(i)])
  }
  return(W)
}

U <- matrix( 
   c(0.1641, 0.2443, 0.6278, 0.1070, -0.2604, -0.8017,
     -0.5389, 0.4277, 0.4637, -0.1373, 0.0752, -0.2904),
   nrow=6,               
   ncol=2,               
   byrow = TRUE)  

means <- c(1.75, 1.75, 1.25, 2, 2, 1)

#Note that weight_vector() subtracts the means
#delta-like weight matrix
M <- c(1, -1, 1, -1, -1, 1)
W1 <- weight_vector(M, U, means)

M <- c(-2, 2, 2, -1, -2, 2)
W2 <- weight_vector(M, U, means)

M <- c(1, 3, 0, 1, 3, 1)
W3 <- weight_vector(M, U, means)

M <- c(2, 3, 1, 1, -2, 0)
W4 <- weight_vector(M, U, means)

malware_delta <- cbind(W1, W2, W3, W4)


#benign samples
B <- c(-1, 2, 1, 2, -1, 0)
W1 <- weight_vector(B, U, means)

B <- c(-2, 1, 2, 3, 2, 1)
W2 <- weight_vector(B, U, means)

B <- c(-1, 3, 0, 1, 3, -1)
W3 <- weight_vector(B, U, means)

B <- c(0, 2, 3, 1, 1, -2)
W4 <- weight_vector(B, U, means)

benign_delta <- cbind(W1, W2, W3, W4)


#calculate weight vector
#Y1
Y <- c(1, 5, 1, 5, 5, 1)
W <- weight_vector(Y, U, means)
print(paste("Y1: ", classify(W, malware_delta, benign_delta, means)))

#Y2
Y <- c(-2, 3, 2, 3, 0, 2)
W <- weight_vector(Y, U, means)
print(paste("Y2: ", classify(W, malware_delta, benign_delta, means)))

#Y3
Y <- c(2, -3, 2, 3, 0, 0)
W <- weight_vector(Y, U, means)
print(paste("Y3: ", classify(W, malware_delta, benign_delta, means)))


Y <- c(2, -2, 2, 2, -1, 1)
W <- weight_vector(Y, U, means)
print(paste("Y4: ", classify(W, malware_delta, benign_delta, means)))
