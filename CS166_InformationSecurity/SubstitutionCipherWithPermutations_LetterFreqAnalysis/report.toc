\contentsline {section}{\numberline {1}Simple Substitution Cipher with Permutations}{1}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Language Choice}{1}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}Encryption and Decryption}{1}{subsection.1.2}% 
\contentsline {subsection}{\numberline {1.3}Inputs and Outputs}{1}{subsection.1.3}% 
\contentsline {section}{\numberline {2}Letter Frequency Analysis}{1}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Language Choice}{1}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Inputs and Outputs}{2}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Frequency Analysis}{2}{subsection.2.3}% 
\contentsline {subsubsection}{\numberline {2.3.1}Chi-Squared Test}{2}{subsubsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.4}Experiments}{2}{subsection.2.4}% 
\contentsline {subsubsection}{\numberline {2.4.1}Simple Frequency Analysis}{2}{subsubsection.2.4.1}% 
\contentsline {subsubsection}{\numberline {2.4.2}Scored Frequency Analysis}{4}{subsubsection.2.4.2}% 
\contentsline {section}{\numberline {3}Results}{5}{section.3}% 
\contentsline {section}{\numberline {4}Improvements}{6}{section.4}% 
\contentsline {section}{\numberline {5}Code Listings}{6}{section.5}% 
\contentsline {subsection}{\numberline {5.1}SimpleSubPermute.cpp}{6}{subsection.5.1}% 
\contentsline {subsection}{\numberline {5.2}Haxor.java}{9}{subsection.5.2}% 
