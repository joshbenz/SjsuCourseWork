#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <memory>
#include <iterator> 
#include <map>
#include <vector>
#include <algorithm>

using std::string;
using std::cout;

//English letter frequencies A, B, C, ..., Z
const double ENGLISH_FREQS[26] = { 0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015,
                                    0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749,
                                    0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758,
                                    0.00978, 0.02360, 0.00150, 0.01974, 0.00074 };

//Took me a while before I realized it is way easier to relate the freqs when its sorted
const double SORTED_FREQS[26] = { 0.12702, 0.09056, 0.08167, 0.07507, 0.06966, 0.06749, 0.06327,
                                    0.06094, 0.05987, 0.04253, 0.04025, 0.02782, 0.02758, 0.02406,
                                    0.02360, 0.02228, 0.02015, 0.01974, 0.01929, 0.01492, 0.00978,
                                    0.00772, 0.00153, 0.00150, 0.00095, 0.00074 };

char SORTED_CHARS[26] = { 'E', 'T', 'A', 'O', 'I', 'N', 'S', 'H', 'R', 'D', 'L', 'C',
                                    'U', 'M', 'W', 'F', 'G', 'Y', 'P','B', 'V', 'K', 'J', 'X', 'Q', 'Z' };

//I should have just used a map

//likelyhood of a space appearing next to a char. Again order is A, B, C, ..., Z
const double SPACE_FREQS[27] = { 0.0651738, 0.0124248, 0.0217339, 0.0349835, 0.1041442, 0.0197881, 0.0158610, 0.0492888,
                                    0.0558094, 0.0009033, 0.0050529, 0.0331490, 0.0202124, 0.0564513, 0.0596302, 0.0137645,
                                    0.0008606, 0.0497563, 0.0515760, 0.0729357, 0.0225134, 0.0082903, 0.0171272, 0.0013692,
                                    0.0145984, 0.0007836, 0.1918182 };

const string DIGRAPHS[12] = { "TH", "HE", "AN", "IN", "ER", "ON", "RE", "ED", "ND", "HA", "AT", "EN" };
const string TRIGRAPHS[13] = { "THE", "AND", "THA", "ENT", "ION", "TIO", "FOR", "NDE", "HAS", "NCE", "TIS", "OFT", "MEN" };
const string DOUBLES[7] = { "SS", "EE", "TT", "FF", "LL", "MM", "OO" };




int* charCounts(string text, int size)
{
    //int* counts = new int[size]{0};
    int* counts = new int[size]{0};
    for(int i=0; i<text.length(); i++) {
        counts[static_cast<int>(text[i])]++;
    }
    return counts;
}

double* charFreqs(int* counts, int size) {
    double* freqs = new double[size]{0};
    for(int i=0; i<size; i++) {
        freqs[i] = (double)counts[i] / (double)size;
    }

    return freqs;
}

int totalCharCount(int *counts, int size) 
{
    int total = 0;
    for(int i=0; i<size; i++) {
        if(counts[i] > 0) {
            total += counts[i];
        }
    }
    return total;
}

int* englishLetterCounts(string text) {
    int* counts = new int[26]{0};
    for(int i=0; i<text.length(); i++) {
        if(text[i] >= 65 && text[i] <= 90) {
            counts[static_cast<int>(text[i]) - 65]++;
        } else if(text[i] >= 97 && text[i] <= 122) {
            counts[static_cast<int>(text[i]) - 97]++;
        } else {}
    }
    return counts;
}

double chiSquared(string text)
{
    int* letterCounts = englishLetterCounts(text);
    int counts = totalCharCount(letterCounts, 26);
    int len = counts - (text.length() - counts);

    double chi = 0.0;
    for(int i=0; i<26; i++) {
        double observed = letterCounts[i];
        double expected = len * ENGLISH_FREQS[i];
        double difference = observed - expected;
        chi += difference * difference / expected;
    }
    delete(letterCounts);
    return chi;
}

char tryChar(double freq) {
    int index = 0;

    while(freq < SORTED_FREQS[index]){
        index++;
    }
    return SORTED_CHARS[index];
}

string freqAnalysis(string ciphertext) 
{
    int* counts = charCounts(ciphertext, 128);
    double* ciphertextFreqs = charFreqs(counts, 128);

    string s = "";

    for(int i=0; i<ciphertext.length(); i++) {
        s += tryChar(ciphertextFreqs[static_cast<int>(ciphertext[i])]);
    }

    delete ciphertextFreqs;
    delete counts;

    cout << chiSquared(s) << "\n";
    return s;
}

string digraphs(string text)
{
    std::vector<string> v;
    for(int i=0; i<text.length(); i++) {
        string s = "";
        s += text[i]; i++;
        s += text[i];

        if(s.length() > 0) v.push_back(s);
    }

    int counter = 0;
    string construct = "";
    double bestSoFar = 100000000;
    string bestStringSoFar = "";
    for(auto &i : v) {
        construct = "";
        string s = i;
        
        for(auto &j : v) {
            if(j == s) {
                construct += DIGRAPHS[counter];
            } else {
                construct += j;
            }
        }
        counter++;
        double test = chiSquared(construct);
        if(test < bestSoFar) {
            bestSoFar = test;
            bestStringSoFar = construct;
            cout << bestSoFar << ": " << bestStringSoFar << "\n";
        }
    }
    return bestStringSoFar;

}

//arg 1: filename of ciphertext
int main(int argc, char* argv[]) 
{
    if(argc <= 1) {
        cout << "Missing some args! \n";
        return 0;
    }

    std::ifstream file;
    file.open(argv[1]);

    std::stringstream sStream;
    sStream << file.rdbuf();
    string ciphertext = sStream.str();
    sStream.str("");
    file.close();

    string s = freqAnalysis(ciphertext);
    cout << "After freq analysis " << s << "\n";
    string p = digraphs(s);

    return 0;
}