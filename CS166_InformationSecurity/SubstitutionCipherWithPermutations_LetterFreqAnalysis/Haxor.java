import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.lang.Math;

public class Haxor {
    private static Map<String, Double> ENGLISH_FREQS;
    private static List<String> sortedChars;
    final static double DELTA = 0.01;

    private static void init() {
        //load English Frequencies
        ENGLISH_FREQS = new HashMap<>();
        ENGLISH_FREQS.put(" ", 17.166);
        ENGLISH_FREQS.put("E", 12.702);
        ENGLISH_FREQS.put("T", 9.056);
        ENGLISH_FREQS.put("A", 8.167);
        ENGLISH_FREQS.put("O", 7.507);
        ENGLISH_FREQS.put("I", 6.966);
        ENGLISH_FREQS.put("N", 6.749);
        ENGLISH_FREQS.put("S", 6.327);
        ENGLISH_FREQS.put("H", 6.094);
        ENGLISH_FREQS.put("R", 5.987);
        ENGLISH_FREQS.put("D", 4.253);
        ENGLISH_FREQS.put("L", 4.025);
        ENGLISH_FREQS.put("C", 2.782);
        ENGLISH_FREQS.put("U", 2.758);
        ENGLISH_FREQS.put("M", 2.406);
        ENGLISH_FREQS.put("W", 2.360);
        ENGLISH_FREQS.put("F", 2.228);
        ENGLISH_FREQS.put("G", 2.0015);
        ENGLISH_FREQS.put("Y", 1.974);
        ENGLISH_FREQS.put("P", 1.929);
        ENGLISH_FREQS.put("B", 1.492);
        ENGLISH_FREQS.put("V", 0.978);
        ENGLISH_FREQS.put("K", 0.772);
        ENGLISH_FREQS.put("J", 0.153);
        ENGLISH_FREQS.put("X", 0.150);
        ENGLISH_FREQS.put("Q", 0.095);
        ENGLISH_FREQS.put("Z", 0.074);

        sortedChars = new ArrayList<>();
        sortedChars.add(" ");
        sortedChars.add("E");
        sortedChars.add("T");
        sortedChars.add("A");
        sortedChars.add("O");
        sortedChars.add("I");
        sortedChars.add("N");
        sortedChars.add("S");
        sortedChars.add("H");
        sortedChars.add("R");
        sortedChars.add("D");
        sortedChars.add("L");
        sortedChars.add("C");
        sortedChars.add("U");
        sortedChars.add("M");
        sortedChars.add("W");
        sortedChars.add("F");
        sortedChars.add("G");
        sortedChars.add("Y");
        sortedChars.add("P");
        sortedChars.add("B");
        sortedChars.add("V");
        sortedChars.add("K");
        sortedChars.add("J");
        sortedChars.add("X");
        sortedChars.add("Q");
        sortedChars.add("Z");
    }

    private static Map<String, Integer> textLetterCounts(String text) {
        Map<String, Integer> map = new HashMap<>();

        for(int i=0; i<text.length(); i++) {
            String c = String.valueOf(text.charAt(i));
            if(!map.containsKey(c)) {
                map.put(c, 1);
            } else {
                int val = map.get(c);
                val++;
                map.put(c, val);
            }
        }
        return map;
    }

    private static List<String> sortMapToList(Map<String, Integer> map) {

        return map.entrySet().stream()
        .sorted(Comparator.comparing(Map.Entry::getValue, Comparator.reverseOrder()))
        .map(Map.Entry::getKey)
        .collect(Collectors.toList());

    } 

    private static int countAllChars(Map<String, Integer> map) {
        int total = 0;
                for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if(Character.isLetter(entry.getKey().charAt(0))) {
                total += entry.getValue();
            }
        } 
        return total;
    }

    private static double chiSquared(String text) {
        Map<String, Integer> map = textLetterCounts(text); //counts for all characters
        int len = countAllChars(map);
        double chi = 0.0;

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if(Character.isLetter(entry.getKey().charAt(0))) {
                double observed = (double)entry.getValue();
                double expected = (double)len * (double)(ENGLISH_FREQS.get(entry.getKey().toUpperCase()) / 100.0);
                double diff = observed - expected;
                chi += diff * diff / expected;
            }
        }   
        return chi;
    }

    private static String round(String text, String cipherChar, String englishChar) {
        String result = new String();

        for(int i=0; i<text.length(); i++) {
            if(String.valueOf(text.charAt(i)).equals(cipherChar)) {
                result += englishChar;
            } else {
                result += String.valueOf(text.charAt(i));
            }
        }
        return result;
    }

    private static Map<String, Double> letterCharFreqs(int len, Map<String, Integer> map) {
        Map<String, Double> m = new HashMap<>();

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            m.put(entry.getKey(), ((double)((double)entry.getValue()/(double)len) *100.0));
        }
        return m;
    }

    //maybe determine a set of possible chars to try
    private static String determineEnglishChar(double cipherFreq) {
        String result = new String("");
        //for (Map.Entry<String, Double> entry : ENGLISH_FREQS.entrySet()) {
        for(String c : sortedChars) {
            if((Math.abs((ENGLISH_FREQS.get(c)/100.0) - cipherFreq/100.0) < DELTA)) {
                result += c;
            }
        }
        return result;
    }

    private static void simpleFreqAnalysis(String ciphertext) {
        Map<String, Integer> letterCounts = textLetterCounts(ciphertext);
        Map<String, Double> cipherFreqs = letterCharFreqs(ciphertext.length(), letterCounts);
        List<String> sortedTextLetters = sortMapToList(letterCounts);

        int counter = 0;
        String attempt = ciphertext;

        for(String c : sortedTextLetters) {
            //String possible = determineEnglishChar(cipherFreqs.get(c));
            //System.out.print("Possible Chars to try for (" + c + "):  " + possible + "\n");
            if(counter < 27) {
             System.out.print("Trying: " + c + "  replacing with: " + sortedChars.get(counter) + "     ");
            attempt = round(attempt, c, sortedChars.get(counter));
            System.out.println(chiSquared(attempt) + ": " + attempt);
            counter++;
            }
        }
        System.out.println(attempt + "\n");

    }

        private static String scoredFreqAnalysis(String ciphertext) {
        //List<String> bestChiScoredAttempts = new ArrayList<>();
        String usedLetters = new String("");

        Map<String, Integer> letterCounts = textLetterCounts(ciphertext);
        Map<String, Double> cipherFreqs = letterCharFreqs(ciphertext.length(), letterCounts);
        List<String> sortedTextLetters = sortMapToList(letterCounts);

        int counter = 0;
        String attempt = ciphertext;

        for(String c : sortedTextLetters) {
            String possible = determineEnglishChar(cipherFreqs.get(c));
            System.out.print("Possible Chars to try for (" + c + "):  " + possible + "\n");
            String test = new String(attempt);

            double bestChiSoFar = 10000000.0;
            String bestAttempt = attempt;
            String bestChar = "";

            for(int i=0; i<possible.length(); i++) {
                if(!usedLetters.contains(String.valueOf(possible.charAt(i)))) {
                    System.out.print("Trying: " + c + "  replacing with: " + String.valueOf(possible.charAt(i)) + "     ");
                    test = round(attempt, c, String.valueOf(possible.charAt(i)));

                    double testChi = chiSquared(test);
                    System.out.println(testChi + ": " + test);

                    if(testChi < bestChiSoFar) {
                        bestChiSoFar = testChi;
                        bestAttempt = test;
                        bestChar = String.valueOf(possible.charAt(i));
                    }
                }
            }
            attempt = bestAttempt; 
            usedLetters += bestChar;
        }
        return attempt;
    }


    public static void main(String[] args) {

        //load ciphertext
        String ciphertext = new String();
        try {
            ciphertext = new String(Files.readAllBytes(Paths.get("ciphertext.txt")));
        } catch(IOException e) {}

        init();
        simpleFreqAnalysis(ciphertext);
        String text = scoredFreqAnalysis(ciphertext);
        System.out.println("\n The best Scored: " + text + " Chi-Squared Score: " + chiSquared(text));


    }
}