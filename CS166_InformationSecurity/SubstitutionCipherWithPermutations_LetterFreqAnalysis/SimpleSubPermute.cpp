#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cstdlib>

using std::cout;
using std::string;

//n # of next lexicographically greater permutations
string permute(string s, int n) 
{
    for(int i=0; i<n; i++) {
        std::next_permutation(s.begin(), s.end());
    }
    return s;
}

//n # of previous lexicographically greater permutations
string prevPermute(string s, int n) 
{
    for(int i=0; i<n; i++) {
        std::prev_permutation(s.begin(), s.end());
    }
    return s;
}


//substitution by shifting use 128 to allow for any ASCII character
string shift(string plaintext, int nShift) 
{
    string shiftedText = "";
    for(int i=0; i<plaintext.length(); i++) {
        shiftedText += (char)((int)(plaintext[i] + nShift - ' ') % 128 + ' ');
    }
    return shiftedText;
}

//encrypt by shifting and then permuting
string encrypt(string plaintext, int nShift, int permutes) 
{
    return permute(shift(plaintext, nShift), permutes);
}

//decrypt by permuting and then shifting back
string decrypt(string cipherText, int nShift, int permutes) 
{
    string s = prevPermute(cipherText, permutes);
    return shift(s, 128 - (nShift % 128));
}

/*
    arg 1: filename of plaintext
    arg 2: number of shifts 
    arg 3: number of permutations
*/

int main(int argc, char* argv[]) 
{
    if(argc <= 3) {
        cout << "Missing some args! \n";
        return 0;
    }

    std::ifstream file;
    file.open(argv[1]);

    std::stringstream sStream;
    sStream << file.rdbuf();
    string plaintext = sStream.str();
    sStream.str("");
    file.close();

    cout << "Text from file: " + plaintext << "\n";
    string ciphertext = encrypt(plaintext, atoi(argv[2]), atoi(argv[3]));
    cout << "Ciphertext: " + ciphertext << "\n";
    cout << "Decrypted Ciphertext using Decription Algorithm: " + decrypt(ciphertext, atoi(argv[2]), atoi(argv[3])) << "\n";

    //dump it to a file
    std::ofstream out("ciphertext.txt");
    out << ciphertext;
    out.close();
    
    return 0;
}