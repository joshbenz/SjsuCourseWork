import java.net.URL;
import com.vmware.vim25.*;
import com.vmware.vim25.mo.*;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Main {
  public static void main(String[] args) throws Exception {
    System.out.println("CS 218 Spring 2020 HW2 from Joshua Benz");
    ServiceInstance si = 
                      new ServiceInstance(
                      new URL("https://" + args[0] + "/sdk"),
                      args[1],
                      args[2],
                      true);

    Folder root = si.getRootFolder();
    String name = root.getName();

    // Get Host systems
    ManagedEntity[] mes =
                  new InventoryNavigator(root)
                      .searchManagedEntities("HostSystem");

    if(mes == null || mes.length == 0) return;
  
    //enumerate and print host systems
    for(int i=0; i<mes.length; i++) {
      Host host = new Host(mes[i]);
      System.out.println("host[" + i + "]:\n" + host.toString());
    }


    //get VMs Independently of hosts
    mes = new InventoryNavigator(root)
              .searchManagedEntities("VirtualMachine");

    if(mes == null || mes.length == 0) return;

    //Convert to my wrapper class
    List<VM> vms = new ArrayList<VM>();
    for(int i=0; i<mes.length; i++) {
      vms.add(new VM((VirtualMachine)mes[i]));
    }
    toggleAndPrint(vms);
  }

  private static void toggleAndPrint(List<VM> vms) {
    for(int i=0; i<vms.size(); i++) {
      //Read the VM power state
      switch(vms.get(i).powerState()) {
        case poweredOn: {
          TaskResult result = vms.get(i).powerOff();
          System.out.println("VM[" + i + "]");
          System.out.println(vms.get(i).toString(result));
          break;
        } 

        case poweredOff: {
          TaskResult result = vms.get(i).powerOn();
          System.out.println("VM[" + i + "]");
          System.out.println(vms.get(i).toString(result));
          break;
        }

        case suspended: {
          break; //do nothing
        }
      }
    }
  }
}

class Host {
  private HostSystem host;
    
  public Host(ManagedEntity h) {
    host = (HostSystem)h;
  }

  public String toString() {
    String result = "Name = " + host.getName() + "\n";

    String buildInfo = host.getConfig().getProduct().getFullName();
    result += "ProductFullName = " + buildInfo + "\n";


    //Build String with datastore info if available
    Datastore[] datastores = this.datastores();
    if(datastores != null) {
      for(int i=0; i<datastores.length; i++) {
        result += "Datastore[" + i + "]: ";
        result += "name=" + datastores[i].getName() + ", ";

        DatastoreSummary info = datastores[0].getSummary();
        result += "capacity = " + info.getCapacity() + ", ";
        result += "FreeSpace = " + info.getFreeSpace() + "\n\n";
        }
      }

    //Build String with network info if available
    Network[] networks = this.networks();
    if(networks != null) {
      for(int i=0; i<networks.length; i++) {
        result += "Network[" + i + "]: ";
        result += "name=" + networks[i].getName() + "\n\n";
      }
    }
    return result;
  }

   /*
    * datastore and network properties are marked as optional
    * and require special privelages, so it possible to not
    * have access to them. If everything goes well, these getters
    * just return them. returns null otherwise.
    *
    */
  public Datastore[] datastores() {
    try {
      return host.getDatastores();
    } catch(RemoteException e) {
      e.printStackTrace();
      return null;
    }
  }

  public Network[] networks() {
    try {
      return host.getNetworks();
    } catch(RemoteException e) {
      e.printStackTrace();
      return null;
    }
  }
}

class VM {
  private VirtualMachine vm;

  VM(VirtualMachine vm) { 
    this.vm = vm;
  }
  public VirtualMachine vm() {return vm;}

  public String toString(TaskResult taskResult) {
    String result = "Name = " + vm.getName() + "\n";
    result += "GuestOS = " + vm.getConfig().getGuestFullName() + "\n";

    VirtualHardware hardwareInfo = vm.getConfig().getHardware();
    result += "CPUs = " + hardwareInfo.getNumCPU() + "\n";
    result += "Memory = " + hardwareInfo.getMemoryMB() + " MB\n";

    GuestInfo info = vm.getGuest();
    result += "Guest State = " + info.getGuestState() + "\n";

    //VirtualMachineToolsRunningStatus ENUM
    //http://www.yavijava.com/docs/vim.vm.GuestInfo.ToolsRunningStatus.html
    result += "Tool running status = " + info.getToolsRunningStatus() + "\n";

    //get power state
    switch(this.powerState()) {
      case poweredOn: {
        result += "Power State = poweredOn\n";
        result += "Power off VM: " + taskResult.toString();
        break;
      } 

      case poweredOff: {
        result += "Power State = poweredOff\n";
        result += "Power on VM: " + taskResult.toString();;
        break;
      }

      case suspended: {
        break; //do nothing
      }

      default: {
        break; //also do nothing
      }
    }

    return result;
  }

  //VirtualMachinePowerState ENUM
  //http://www.yavijava.com/docs/vim.VirtualMachine.PowerState.html
  public VirtualMachinePowerState powerState() {
    VirtualMachineRuntimeInfo runtimeInfo = vm.getRuntime();
    return runtimeInfo.getPowerState();
  }

  public TaskResult powerOn() {
    try {
      Task task = vm.powerOnVM_Task(null);
      return progress(task);
    } catch(RemoteException e) {
      e.printStackTrace();
      return null;
    } 
  }

  public TaskResult powerOff() {
    try {
      //check if we VMware tools installed
      if(vm.getGuest().getToolsRunningStatus()
          .equals(VirtualMachineToolsRunningStatus.guestToolsRunning)) {

        //we can tell the OS to shutdown
        Calendar start = Calendar.getInstance();
        vm.shutdownGuest();
        //wait until we reached hte poweredOff state
        while(this.powerState().equals(VirtualMachinePowerState.poweredOff)) {}
        Calendar complete = Calendar.getInstance();

        return new TaskResult(true, start, complete);
      }

      Task task = vm.powerOffVM_Task();
      return progress(task);
    } catch(RemoteException e) {
      e.printStackTrace();
      return null;
    } 
  }

  private TaskResult progress(Task task) { 
    boolean makingProgress = true; 
    while(makingProgress) {
      try {
        //spin the wheels and check for errors
        switch(task.getTaskInfo().getState()) {
          case success : {
            return new TaskResult(true, 
                                  task.getTaskInfo().getStartTime(),
                                  task.getTaskInfo().getCompleteTime());
          }

          case error : {
            return new TaskResult(false,
                                  task.getTaskInfo().getReason().toString(),
                                  task.getTaskInfo().getStartTime(),
                                  task.getTaskInfo().getCompleteTime());
          }

          case queued: {
            //http://www.yavijava.com/docs/vim.TaskInfo.State.html
            //waiting for free threads
            //do nothing
          }

          case running: {
            //Still in progress
            //do nothing
          }
        }
      } catch(RemoteException e) {
        e.printStackTrace();
        return null;
      }
    }

    //somehting went wrong
    return null;
  }
}

//I only care if it succeeded or failed, 
//the other two cases, I just wait.
class TaskResult {
  boolean success;
  String errorReason;
  Calendar start;
  Calendar complete;

  TaskResult(boolean state,
              Calendar start,
              Calendar complete) {
    success = state;
    this.start = start;
    this.complete = complete;

  }

  TaskResult(boolean state, 
              String err,
              Calendar start,
              Calendar complete) {
    success = state;
    errorReason = err;
    this.start = start;
    this.complete = complete;
  }

  public String toString() {
    String result = "status = ";

    if(success) {
      result += "success, ";
    } else {
      result += errorReason + ", ";
    }
    SimpleDateFormat hwFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    result += "start time = " + hwFormat.format(start.getTime()) + ", ";
    result += "completion time = " + hwFormat.format(complete.getTime()) + "\n";
    return result;
  }
}

