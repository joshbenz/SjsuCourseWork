import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.gson.*;



@WebServlet(
    name = "Customer Endpoints",
    urlPatterns = {"/customer/*"}
)

public class Customer extends HttpServlet {
	private Gson gson;
	private static final String COLLECTION = "customers";
	
	class CustomerModel {
		int id;
		String firstName, lastName;
		
		public CustomerModel(int id, String first, String last) {
			this.id = id;
			firstName = first;
			lastName = last;
		}
		
		public CustomerModel(QueryDocumentSnapshot d) {
			this.id = Integer.parseInt(d.getString("id"));
			firstName = d.getString("firstName");
			lastName = d.getString("lastName");
		}
	}
	
	public Customer() {
		super();
		gson = new Gson();
	}
	
	
	/*
	* GET/JavaViewer/customer/
	* GET/JavaViewer/customer/id 
	*/
	@Override
 	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String urlPath = request.getPathInfo();
		PrintWriter writer = response.getWriter();
		
		//Get all
		if(urlPath == null || urlPath.equals("/")) {
			try {
				List<QueryDocumentSnapshot> documents = DbUtils.get(COLLECTION);
				//not in the db
				if(documents.size() < 1) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
					return;
				}
				
				List<CustomerModel> reply = new ArrayList<>();
				
				for(QueryDocumentSnapshot doc : documents) {
					reply.add(new CustomerModel(doc));
				}
				
				response.setStatus(HttpServletResponse.SC_OK);
				writer.write(gson.toJson(reply));
				writer.flush();
				return;
			} catch (InterruptedException | ExecutionException e) {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				e.printStackTrace();
				return;
			}
		}
		
		//Get by id
		String[] pathSplits = urlPath.split("/");
		if(pathSplits.length == 2) {
			try {
				List<QueryDocumentSnapshot> docs = DbUtils.get(COLLECTION, Integer.parseInt(pathSplits[1]));
				
				//not in the db
				if(docs.size() < 1) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
					return;
				}
				
				CustomerModel reply = new CustomerModel(docs.get(0));
				
				response.setStatus(HttpServletResponse.SC_OK);
				writer.write(gson.toJson(reply));
				writer.flush();
				return;
			} catch (NumberFormatException | InterruptedException | ExecutionException e) {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				e.printStackTrace();	
			}
			
		} else { //bad url
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;	
		}
		
		//Otherwise something went wrong
		response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return;	
	}
}