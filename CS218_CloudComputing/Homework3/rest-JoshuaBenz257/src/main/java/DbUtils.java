import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class DbUtils {
	private static Firestore db = null;
	private static final String projectId = "joshuabenz257";
	
	public static Firestore db() {
		if(db == null) {
			//db = FirestoreOptions.getDefaultInstance().getService();
			
		    FirestoreOptions firestoreOptions =
		            FirestoreOptions.getDefaultInstance().toBuilder()
		                .setProjectId(projectId)
		                .build();
		        db = firestoreOptions.getService();
		        System.out.println(db);
		}
		return db;
	}
	
	public static List<QueryDocumentSnapshot> get(String collection) 
			throws InterruptedException, ExecutionException {
		
		ApiFuture<QuerySnapshot> query = db().collection(collection).get();
		QuerySnapshot querySnapshot = query.get();
		return querySnapshot.getDocuments();
	}
	
	public static List<QueryDocumentSnapshot> get(String collection, int id) 
			throws InterruptedException, ExecutionException {
		
		ApiFuture<QuerySnapshot> query = db().collection(collection)
				.whereEqualTo("id", id)
				.get();
		QuerySnapshot querySnapshot = query.get();
		return querySnapshot.getDocuments();
	} 

}
