import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.*;

@WebServlet(
    name = "Department Endpoints",
    urlPatterns = {"/department/*"}
)

public class Department extends HttpServlet {
	private Gson gson = null;
	class DepartmentModel {
		int id;
		String name;
		float budget;	
		
		public DepartmentModel(int id, String name, float budget) {
			this.id = id;
			this.name = name;
			this.budget = budget;
		}
	}
	
	public Department() {
		super();
		gson = new Gson();
	}
	
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {

    response.setContentType("text/plain");
    response.setCharacterEncoding("UTF-8");

    response.getWriter().print("Hello App Engine!adfasfsaf\r\n");

  }
}